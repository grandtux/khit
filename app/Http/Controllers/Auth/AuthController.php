<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Laravel\Socialite\Facades\Socialite;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */

    public function redirectToProviderf()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallbackf()
    {
        try {
            $user = Socialite::driver('facebook')->user();
        } catch (Exception $e) {
            return Redirect::to('auth/facebook');
        }

        $authUser = $this->findOrCreateUserf($user);

//        if ($authUser->active == 0){
//            $authUser->active = 1;
//            $authUser->save();
//        }

        Auth::login($authUser, true);

        return redirect('/');
    }

    private function findOrCreateUserf($faebookUser)
    {
        if ($authUser = User::where('facebook_id', $faebookUser->id)->first()) {

            return $authUser;
        }

        return User::create([
            'name' => $faebookUser->name,
//            'email' => $faebookUser->email,
            'facebook_id' => $faebookUser->id,
            'avatar' => $faebookUser->avatar,
            'active' => true
        ]);
    }
    protected function create(array $data)
    {
        $activation_code = str_random(30);

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'activation_code' => $activation_code

        ]);
    }
}
