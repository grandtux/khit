<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Prepaid;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;

class PrepaidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cards = Prepaid::orderBy('id', 'desc')->paginate(20);
        return view("prepaid.index",compact('cards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cards = Prepaid::all()->groupBy('type');
        return view("prepaid.create",compact('cards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newcard = new Prepaid();
        $newcard->type = $request->input('type');
        $newcard->remark = $request->input('remark');
        Image::make(\Input::file('image'))->resize(260, 140)->save();
        $image = $request->file('image');
        $despath = 'uploads/images/';
        $filename = $image->getClientOriginalName();
        $image->move($despath, $filename);
        $newcard->file = $despath . $filename;
        /*
         * Sample
         */
        if ($request->hasFile('sample')) {
            Image::make(\Input::file('sample'))->resize(260, 140)->save();
            $sample = $request->file('sample');
            $samplename = $sample->getClientOriginalName();
            $sample->move($despath, $samplename);
            $newcard->sample = $despath . $samplename;
        }
        $newcard->save();
        return redirect('prepaid/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prepaid = Prepaid::find($id);
        $prepaid->forceDelete();
        return redirect('prepaid');
    }

    public function lists()
    {
        $ppcards = Product::where(config('type.PrepaidCards'))->where('status', 1)->get();
        return view('product.lists',compact('ppcards'));

    }
}
