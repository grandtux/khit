<?php

namespace App\Http\Controllers;

use App\Product;
use App\Slider;
use App\User;
use App\Tag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'allpost', 'searchaction', 'category', 'product_tag']]);

    }

    public function index()
    {
        $product = Product::where('status', 1)
            ->orderBy('updated_at')
            ->paginate(20);

        $ebook = Product::where(config('type.Art&Ebooks'))->where('status', 1)->orderBy('updated_at', 'desc')->paginate(3);
        $mmovie = Product::where(config('type.Videos'))->where('status', 1)->orderBy('updated_at', 'desc')->paginate(3);
        $mmusic = Product::where(config('type.Audios'))->where('status', 1)->orderBy('updated_at', 'desc')->paginate(3);
        $tech = Product::where(config('type.Other'))->where('status', 1)->orderBy('updated_at', 'desc')->paginate(3);
        $msub = Product::where(config('type.MyanmarSubtitles'))->where('status', 1)->orderBy('updated_at', 'desc')->paginate(3);
        $ppcards = Product::where(config('type.PrepaidCards'))->where('status', 1)->orderBy('updated_at', 'desc')->paginate(3);
        $slider = Slider::all();

        return view('product.view')
            ->with('ebooks', $ebook)
            ->with('mmovies', $mmovie)
            ->with('mmusics', $mmusic)
            ->with('techs', $tech)
            ->with('msubs', $msub)
            ->with('ppcards', $ppcards)
            ->with('sliders', $slider);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::orderBy('name')->lists('name', 'id');
        return view("product.create", compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ProductRequest $request)
    {

        $products = new Product();
        $products->name = $request->input('name');
        $products->type = $request->input('type');
        $products->category = $request->input('category');
        $products->points = $request->input('points');
        $products->urls = $request->input('url');
        $products->other = $request->input('other');
        $products->uploadby = $request->user()->id;

        $this->validate($request, [
            'name' => 'required|max:255|unique:products',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'file' => 'required|mimes:jpeg,jpg,png,gif,svg,pdf,psd,mp3,mpga,wav,mid,midi,mp4,txt,srt,text,zip,apk,ttf',
            'sample' => 'mimes:jpeg,jpg,png,gif,svg,pdf,psd,mp3,mpga,wav,mid,midi,mp4,txt,srt,text,zip,apk,ttf',
        ]);

        Image::make(\Input::file('image'))->resize(260, 140)->save();
        $image = $request->file('image');
        $despath = 'uploads/images/';
        $filename = $image->getClientOriginalName();
        $image->move($despath, $filename);
        $products->media = $despath . $filename;


        $file = $request->file('file');
        $products->size = $file->getSize();
        $despath = 'uploads/files/';
        $filename = $file->getClientOriginalName();
        $file->move($despath, $filename);
        $products->file = $despath . $filename;

        if ($request->hasFile('sample')) {
            $sample = $request->file('sample');
            $despath = 'uploads/samples/';
            $samplename = $sample->getClientOriginalName();
            $sample->move($despath, $samplename);
            $products->sample = $despath . $samplename;
        }

        if ($request->user()->role == "admin") {
            $products->status = true;
        }
        $products->save();
        $tagsIds = $request->input('tag_list');
        /**
         * Add the lists of tags into the database.
         *
         */
        $products->tags()->attach($tagsIds);
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        if ($product->status == false) {
            return redirect('/');
        }
        return view("product.show", compact('product'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        if($product->uploadby != Auth::user()->id)
        {
            return redirect('/');
        }
        $tags = Tag::orderBy('name')->lists('name', 'id');
        $taglists = $product->getTagListAttribute();
        return view('product.edit', compact('product', 'tags', 'taglists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        if($product->uploadby == Auth::user()->id)
        {
            $product->name = $request->input('name');
            $product->type = $request->input('type');
            $product->category = $request->input('category');
            $product->urls = $request->input('url');
            $product->other = $request->input('other');
            $product->points = $request->input('points');


            $this->validate($request, [
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
                'file' => 'mimes:jpeg,jpg,png,gif,svg,pdf,psd,mp3,mpga,wav,mid,midi,mp4,txt,srt,text,zip,apk,ttf',
                'sample' => 'mimes:jpeg,jpg,png,gif,svg,pdf,psd,mp3,mpga,wav,mid,midi,mp4,txt,srt,text,zip,apk,ttf',
            ]);


            if ($request->hasFile('image')) {
                Image::make(\Input::file('image'))->resize(250, 140)->save();
                $file = $request->file('image');
                $despath = 'uploads/images/';
                $filename = $file->getClientOriginalName();
                $file->move($despath, $filename);
                $product->media = $despath . $filename;

            }


            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $product->size = $file->getSize();
                $despath = 'uploads/files/';
                $filename = $file->getClientOriginalName();
                $file->move($despath, $filename);
                $product->file = $despath . $filename;
            }

            if ($request->hasFile('sample')) {
                $sample = $request->file('sample');
                $despath = 'uploads/samples/';
                $samplename = $sample->getClientOriginalName();
                $sample->move($despath, $samplename);
                $product->sample = $despath . $samplename;
            }

            if ($request->user()->role != "admin") {
                $product->status = false;
            }
            $product->save();
            $tagsIds = $request->input('tag_list');
            /**
             * Delete and Sync the list of tags into the database.
             *
             */
            if (is_null($tagsIds)) {
                $product->tags()->detach();
            } else {
                $product->tags()->sync($tagsIds);
            }

        }

        return redirect('/');


    }

    public function user_post(Request $request, $id)
    {
        $user = $request->user()->id;
        if ($id != $user) {
            return view("errors.503");
        }

        $products = Product::where('uploadby', $user)->orderBy('created_at', 'desc')->get();
        return view('product.ownpost')->with('products', $products)->with('users', $user);
    }

    public function allpost($id)
    {
        $user = User::find($id);
        $title = "Upload By";
        $products = Product::where('uploadby', $id)->where('status', 1)->orderBy('created_at', 'desc')->paginate(12);

        return view('product.uploadby')->with('products', $products)->with('user', $user)->with('title', $title);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function category($cat)
    {
        $product = Product::where('category', $cat)->where('status', 1)->orderBy('created_at', 'desc')->paginate(12);
        return view('category.cattag')
            ->with('products', $product)->with('cat', $cat);


    }

    public function destroy($id)
    {
        $product = Product::find($id);

        \File::delete(public_path($product->file));

        \File::delete(public_path($product->media));


        $product->delete();

        return redirect()->action("ProductController@user_post",[ Auth::id()]);
    }


    /**
     * Publishing the product.
     * @param  int $id
     */
    public function publish($id)
    {
        $product = Product::find($id);
        $product->status = true;
        $product->save();
        return redirect('admin');
    }

    public function getsearch(Request $request)
    {
        $search = $request->get('search');

        $products = Product::where('name', 'like', "%$search%")
            ->paginate(15)
            ->appends(['search' => $search]);
        return view('search_show')->with('products', $products);
    }

    public function product_tag($id)
    {
        $tag = Tag::find($id);
        $product = $tag->products()->where('status', 1)->orderBy('created_at', 'desc')->paginate(12);
        return view('category.cattag')
            ->with('products', $product)->with('tag', $tag->name);
    }


    
}
