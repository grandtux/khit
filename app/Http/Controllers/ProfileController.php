<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "OK update";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($id != Auth::user()->id) {
            return view("errors.503");

        }
        else{
            $user = User::find($id);
            return view("user.profile_show", compact('user'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($id != Auth::user()->id) {
            return view("errors.503");
        } else
        {
            if(Auth::user()->facebook_id != null)
            {
                return view("errors.503");
            }
            else
            {
                $user = User::find($id);
                return view("user.profile_edit")->with("user", $user);
            }
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($id != Auth::user()->id)
        {
            return view("errors.503");
        } else
        {
            if(Auth::user()->facebook_id != null)
            {
                return view("errors.503");
            }
            else
            {
                $user = User::find($id);
                $user->name = $request->input('name');
                $user->address = $request->input('address');
                $user->phoneno = $request->input('phoneno');
                $password = $request->input('password');
                if($password != "")
                {
                    $user->password = bcrypt($password);
                }
                $user->save();
                return redirect('profile/'.$id);
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
