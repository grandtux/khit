<?php

namespace App\Http\Controllers;

use App\Prepaided;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use App\User;
use App\Purchase;
use App\Prepaid;
use Carbon\Carbon;

class PurchaseController extends Controller
{
    public function index($id)
    {

        try {
            //Start the transaction.
            DB::beginTransaction();
            /**
             * Save the record to the purchase table.
             */
            $product = Product::find($id);
            $buyer = User::find(Auth::user()->id);
            $seller = User::find($product->author->id);
            $mypurchase = Purchase::where('product_id',$product->id)->where('buyer_id',$buyer->id)
                ->where('seller_id',$seller->id)->first();
            $file = $product->file;
            if(!$mypurchase || $mypurchase->product->type == "prepaid")
            {
                if(Product::find($id)->points > Auth::user()->points)
                {
                    return redirect('points');
                }
                $purchase = new Purchase();
                $purchase->buyer_id = $buyer->id;
                $purchase->seller_id = $seller->id;
                $purchase->product_id = $product->id;
                $purchase->product_name = $product->name;
                $purchase->getpoints = ($product->points / 4) * 3;
                $purchase->commission = $product->points / 4;
                /**
                 * Reduce the points from buyer
                 */
                $buyer->points = $buyer->points - $product->points;
                /**
                 * Add the points to the seller.
                 */
                $seller->points = $seller->points + $purchase->getpoints;
                $seller->save();
                $purchase->save();
            }
            if ($product->type == "onetime")
            {
                $product->status = false;
            }
            elseif ($product->type == "subtitle")
            {
                if ($buyer->download >= 10)
                {
                    throw new Exception('Subtitle');
                }
                $buyer->download++;
            }

            $buyer->save();
            if($product->type == "prepaid")
            {
                $newcard = Prepaid::where('type',$product->name)->first();
                if($newcard != null)
                {
                    $product->file = $newcard->file;
                    $product->sample = $newcard->sample;
                    $product->other = $newcard->remark;
                    $newcard->delete();
                }
                else
                {
                    $product->status = false;
                }
                $prepaided = new Prepaided();
                $prepaided->user_id = $buyer->id;
                $prepaided->product_id = $product->id;
                $prepaided->file = $file;
                $prepaided->save();
            }

            $product->save();



            //Commit the transaction.
            DB::commit();
            return response()->download($file);
        } catch (Exception $e) {
            //Rollback the transaction if any part fails.
            DB::rollback();
            if($e->getMessage()=='Subtitle')
            {
                return redirect('/')->with([
                    'flash_message' => 'Downloads for today is finished! Try again tomorrow.',
                    'flash_message_important' => true
                ]);
            }
            //Return the user to the checkout form with a generic error.
            return redirect()->route('products.show', [$id])->with([
                'flash_message' => 'Something went wrong please try again later.',
                'flash_message_important' => true
            ]);
        }


    }

    public function special($id)
    {
        if (Auth::user()->role != "VIP") {
            return redirect()->route('products.show', [$id])->with([
                'flash_message' => 'You will need to be Special member for buying this products.',
                'flash_message_important' => true
            ]);
        }
        else
        {
            try {
                //Start the transaction.
                DB::beginTransaction();
                $product = Product::find($id);
                $buyer = User::find(Auth::user()->id);
                $seller = User::find($product->author->id);

                $mypurchase = Purchase::where('product_id',$product->id)->where('buyer_id',$buyer->id)
                    ->where('seller_id',$seller->id)->first();
                if(!$mypurchase)
                {
                    if(Product::find($id)->points > Auth::user()->points)
                    {
                        return redirect('points');
                    }
                    /**
                     * Save the record to the purchase table.
                     */
                    $purchase = new Purchase();
                    $purchase->buyer_id = $buyer->id;
                    $purchase->seller_id = $seller->id;
                    $purchase->product_id = $product->id;
                    $purchase->product_name = $product->name;
                    $purchase->getpoints = ($product->points / 10) * 9;
                    $purchase->commission = $product->points / 10;
                    /**
                     * Reduce the points from buyer
                     */
                    $buyer->points = $buyer->points - $product->points;
                    $buyer->save();
                    /**
                     * Add the points to the seller.
                     */
                    $seller->points = $seller->points + $purchase->getpoints;
                    $seller->save();
                    $purchase->save();
                }

                if ($product->type == "onetimespecial")
                {
                    $product->status = false;
                }
                $product->save();


                //Commit the transaction.
                DB::commit();
                return response()->download($product->file);
            } catch (Exception $e) {
                //Rollback the transaction if any part fails.
                DB::rollback();
                return redirect()->route('products.show', [$id])->with([
                    'flash_message' => 'Something went wrong please try again later.',
                    'flash_message_important' => true
                ]);

            }
        }
    }
    public function sales()
    {
        $sales = Purchase::orderBy('created_at', 'desc')
            ->get();
        return view("product.sale")
            ->with("sales", $sales);

    }

    public function user_sales()
    {
        $sales = Purchase::where('seller_id', Auth::user()->id)
            ->orderBy('created_at', 'desc')
            ->get();
        return view("product.sale")
            ->with("sales", $sales);

    }

    public function user_buys()
    {
        $buys = Purchase::where('buyer_id', Auth::user()->id)
            ->orderBy('created_at', 'desc')
            ->get();
        return view("product.purchase")
            ->with("buys", $buys);
    }

    public function check_commissions($id)
    {
        $startofyear = new Carbon('first day of January'.$id);


        $jan = new Carbon($startofyear->startOfMonth());
        $feb  = new Carbon($startofyear->addMonth());
        $mar  = new Carbon($startofyear->addMonth());
        $apr  = new Carbon($startofyear->addMonth());
        $may  = new Carbon($startofyear->addMonth());
        $jun  = new Carbon($startofyear->addMonth());
        $july  = new Carbon($startofyear->addMonth());
        $aug  = new Carbon($startofyear->addMonth());
        $sep  = new Carbon($startofyear->addMonth());
        $oct  = new Carbon($startofyear->addMonth());
        $nov  = new Carbon($startofyear->addMonth());
        $dec  = new Carbon($startofyear->addMonth());
        $nextjan  = new Carbon($startofyear->addMonth());



        $commission['jan'] = Purchase::whereBetween('created_at',[$jan,$feb])->sum('commission');
        $commission['feb'] = Purchase::whereBetween('created_at',[$feb,$mar])->sum('commission');
        $commission['mar'] = Purchase::whereBetween('created_at',[$mar,$apr])->sum('commission');
        $commission['apr'] = Purchase::whereBetween('created_at',[$apr,$may])->sum('commission');
        $commission['may'] = Purchase::whereBetween('created_at',[$may,$jun])->sum('commission');
        $commission['jun'] = Purchase::whereBetween('created_at',[$jun,$july])->sum('commission');
        $commission['july'] = Purchase::whereBetween('created_at',[$july,$aug])->sum('commission');
        $commission['aug'] = Purchase::whereBetween('created_at',[$aug,$sep])->sum('commission');
        $commission['sep'] = Purchase::whereBetween('created_at',[$sep,$oct])->sum('commission');
        $commission['oct'] = Purchase::whereBetween('created_at',[$oct,$nov])->sum('commission');
        $commission['nov'] = Purchase::whereBetween('created_at',[$nov,$dec])->sum('commission');
        $commission['dec'] = Purchase::whereBetween('created_at',[$dec,$nextjan])->sum('commission');


//        $thismonth_commission = Purchase::where('created_at', '>=', Carbon::now()->startOfMonth())->get();
//        $lastmonth_commission = Purchase::where('created_at', '>=', Carbon::now()->subMonth())->get();

        $commission['total'] = Purchase::all()->sum('commission');
//            DB::table('purchases')->sum('commission');
        return view("product.commissions",compact('commission'));

    }

    public function sample($id)
    {
        $product = Product::find($id);
        return response()->download($product->sample);
    }
    public function mypurchases()
    {
        $purchases = Purchase::where('buyer_id', Auth::user()->id)->whereNotNull('product_id')->get();
        $prepaideds = Prepaided::where('user_id', Auth::user()->id)->get();
        $title = "My Purchases";
        return view('purchase.index')->with('purchases', $purchases)->with('title', $title)->with('prepaideds', $prepaideds);

    }
    public function prepaided($id)
    {
        $prepaided = Prepaided::find($id);
        return response()->download($prepaided->file);
    }


}
