<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class RegistrationController extends Controller
{
    public function confirm($activation_code)
    {
        if( ! $activation_code)
        {
            throw new InvalidActivationCodeException;
        }

        $user = User::whereActivationCode($activation_code)->first();

        if ( ! $user)
        {
            throw new InvalidActivationCodeException;
        }

        $user->active = 1;
        $user->activation_code = null;
        $user->save();

        return redirect()->action('Auth\AuthController@getLogin')->with([
            'flash_message' =>'You have successfully verified your account.',
            'flash_message_important' => true

        ]);
    }
}
