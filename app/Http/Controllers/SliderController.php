<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slider=Slider::all();
        return view('admin.slider_show')->with('sliders',$slider);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.slider_create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slider=new Slider();

        $this->validate($request, [

            'pathurl' => 'required|image|mimes:jpeg,png,jpg,gif,svg,pdf',
        ]);

        Image::make(\Input::file('pathurl'))->resize(260, 140)->save();
        $image = $request->file('pathurl');
        $despath = 'upload/images/';
        $filename = str_random(6) . '_' . $image->getClientOriginalName();
        $image->move($despath, $filename);
        $slider->pathurl = $despath . $filename;
        $slider->save();
        return redirect('/slider');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.slider_edit')->with('slider', $slider);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $slider=Slider::find($id);
        $this->validate($request, [

            'pathurl' => 'required|image|mimes:jpeg,png,jpg,gif,svg,pdf',
        ]);

        if ($request->hasFile('pathurl')) {
            Image::make(\Input::file('pathurl'))->resize(1920, 700)->save();
            $file = $request->file('pathurl');
            $despath = 'upload/images/';
            $filename = str_random(6) . '_' . $file->getClientOriginalName();

            $file->move($despath, $filename);
            $slider->pathurl = $despath . $filename;
        }
        $slider->info = $request->input('info');
        $slider->save();
        return redirect('/slider');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
