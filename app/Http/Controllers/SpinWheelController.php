<?php

namespace App\Http\Controllers;

use App\PrizeType;
use App\SpinWheelLog;
use App\SpinWheelRawLog;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SpinWheelController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('spinwheel.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->input('user');

        if($user == \Auth::id())
        {
            $last_record = SpinWheelRawLog::where('user_id',\Auth::id())->orderBy('id','desc')->first();
            $spin_wheel_raw =SpinWheelRawLog::create([
                'user_id'   => $user

            ]);

            if(!!$last_record){
                if(Carbon::parse($last_record->created_at)->addSeconds(10) > Carbon::parse($spin_wheel_raw->created_at)){
                    return response()->json([
                        'status' => 0,
                    ]);

                }
            }
            $random = mt_rand(1,10000);
            $prize_type = PrizeType::where('start_random','<=',$random)->where('end_random','>=',$random)->first();
            $stop_degree = mt_rand($prize_type->start_degree,$prize_type->end_degree);
            $spin_wheel = SpinWheelLog::create([
                'user_id'               => $user,
                'prize_type_id'         => $prize_type->id,
                'spin_wheel_raw_log_id' => $spin_wheel_raw->id,
                'random'                => $random,
                'point'                 => $prize_type->point,
                'degree'                => $stop_degree

            ]);

            $user_info =  \Auth::user();
            $user_info->points += $prize_type->point;
            $user_info->save();

            return response()->json([
                'status' => 1,
                'random' => $stop_degree,
                'user'    => $user
            ]);

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
