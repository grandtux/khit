<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('id', 'desc')->get();
        return view("user.index_data")
            ->with("users", $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = User::findorfail($id);
        $user->updatepoints = User::find($id)->updatepoints;
        return view("user.show", compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

        $user = User::find($id);
        return view("user.edit")->with("user", $user);

    }

    public function editprofile($id)
    {
        $user = User::find($id);
        return view("user.user_profile_edit")->with("user", $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /**
         * Add the record to the updatepoint table.
         */
        $user = User::find($id);

        $points = $request->input('points');
        if ($points != "")
        {
            $admin_id = Auth::id();
            $user->updatepoints()->create([
                'admin_id' => $admin_id,
                'points' => $points
            ]);
            $user->points = $user->points + $points;
        }
        if ($user->role != "admin")
        {
            $role = $request->input('role');
            $user->role = $role;
        }   
        $user->save();
        return view("user.show", compact('user'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user= User::find($id);
        $user->delete();
        $users = User::orderBy('id', 'desc')->get();;
        return view("user.index_data")
            ->with("users", $users);
    }

}

