<?php

namespace App\Http\ViewComposers;

use App\Slider;
use Illuminate\View\View;

class BannerComposer
{
    protected $slider;
    /**
     * Create a latest composer.
     *
     * @return void
     */
    public function __construct(Slider $slider)
    {
        $this->slider = $slider->find(1);

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('slider', $this->slider);
    }
}