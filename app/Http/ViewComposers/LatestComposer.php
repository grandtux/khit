<?php

namespace App\Http\ViewComposers;

use App\Product;
use Illuminate\View\View;

class LatestComposer
{
    protected $products;

    /**
     * Create a latest composer.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->products = $product->orderBy('id', 'desc')->take(3)->get();

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('latests', $this->products);
    }
}