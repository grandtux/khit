<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('auth/facebook', 'Auth\AuthController@redirectToProviderf');

Route::get('auth/facebook/callback', 'Auth\AuthController@handleProviderCallbackf');

Route::get('products/user/{id}', 'ProductController@allpost')->where('id', '[0-9]+');

Route::get('category/{cat}', 'ProductController@category');

Route::get('tag/{id}/products', 'ProductController@product_tag')->where('id', '[0-9]+');

Route::get('/', 'PrepaidController@lists');
//Route::get('/', 'ProductController@index');

Route::resource('products', 'ProductController',['except' => [
    'index'
]]);

Route::get('search', 'ProductController@getsearch');

Route::resource('tag', 'TagController');

Route::resource('page', 'PageController',['except' => [
    'index','create','destroy'
]]);



Route::group(['middleware' => 'auth'], function () {

    Route::get('user/{id}/products', 'ProductController@user_post')->where('id', '[0-9]+');

//    Route::get('product/{id}/purchase', 'PurchaseController@index')->where('id', '[0-9]+');

    Route::post('product/{id}/purchase', 'PurchaseController@index')->where('id', '[0-9]+');

    Route::get('product/{id}/sample', 'PurchaseController@sample')->where('id', '[0-9]+');

//    Route::get('product/{id}/specialpurchase', 'PurchaseController@special')->where('id', '[0-9]+');

    Route::post('product/{id}/specialpurchase', 'PurchaseController@special')->where('id', '[0-9]+');

    Route::get('mypurchases', 'PurchaseController@mypurchases');

    Route::get('prepaided/{id}', 'PurchaseController@prepaided');

    Route::get('user/sales', 'PurchaseController@user_sales');

    Route::get('user/purchases', 'PurchaseController@user_buys');

    Route::resource('profile', 'ProfileController');

    Route::resource('points', 'PointController');




    Route::group(['prefix' => 'messages'], function () {
        Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
        Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
        Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
        Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
        Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
        Route::delete('{id}', ['as' => 'messages.destroy', 'uses' => 'MessagesController@destroy']);
    });


});


Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'RegistrationController@confirm'
]);


Route::group(['middleware' => 'admin'], function () {

    Route::resource('user', 'UserController', ['except' => [
        'create', 'store'
    ]]);
    Route::resource('admin', 'DashboardController', ['only' => ['index']]);

    Route::get('product/{id}/publish', 'ProductController@publish')->where('id', '[0-9]+');

    Route::get('commissions/{id}', 'PurchaseController@check_commissions');

    Route::resource('slider', 'SliderController');

    Route::get('published/products', 'DashboardController@publishedposts');

    Route::resource('prepaid', 'PrepaidController');

    Route::get('updatepoints', 'PointController@all');

    Route::get('sales', 'PurchaseController@sales');

});

Route::resource('spinwheel','SpinWheelController');
