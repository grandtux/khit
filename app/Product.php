<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{


    public function author()
    {
        return $this->belongsTo('App\User', "uploadby");
    }
    /**
     * Get the tags associated with the given posts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }

    /**
     * Get a list of tag ids associated with the current post.
     *
     * @return array
     */

    public function getTagListAttribute()
    {
        return $this->tags->lists('id')->all();
    }
}
