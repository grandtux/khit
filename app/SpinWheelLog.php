<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpinWheelLog extends Model
{
    protected $guarded = ['id'];
}
