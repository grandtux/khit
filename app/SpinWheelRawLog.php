<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpinWheelRawLog extends Model
{
    protected $fillable = ['user_id'];
}
