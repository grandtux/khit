<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Updatepoint extends Model
{
    protected $fillable = ['admin_id','points'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    
}
