<?php
return[

    'all'=>[
        '1'=>"Art&Ebooks",
        '2'=>"Videos",
        '3'=>"Audios",
        '4'=>"Other",
        '5'=>"MyanmarSubtitles",
        '6'=>"PrepaidCards"


    ],

    'type'=>[
        '1'=>"normal",
        '2'=>"special",
        '3'=>"onetime",
        '4'=>"subtitle",
        '5'=>"onetimespecial",
        '6'=>"prepaid"

    ],
    'prepaid'=>[
        '1'=>"MPT top up - 10000 Ks",
        '2'=>"MPT top up - 5000 Ks",
        '3'=>"MPT top up - 3000 Ks",
        '4'=>"MPT top up - 1000 Ks",
        '5'=>"CDMA 800hz top up - 5000 Ks",
        '6'=>"CDMA 800hz top up - 3000 Ks",
        '7'=> "CDMA 800hz top up - 1000 Ks",
        '8'=>"Ooredoo top up - 10000 Ks",
        '9'=>"Ooredoo top up - 5000 Ks",
        '10'=>"Ooredoo top up - 3000 Ks",
        '11'=>"Ooredoo top up - 1000 Ks",
        '12'=>"Ooredoo top up - 500 Ks",
        '13'=>"Telenor top up - 10000 Ks",
        '14'=>"Telenor top up - 6000 Ks",
        '15'=>"Telenor top up - 5000 Ks",
        '16'=>"Telenor top up - 3000 Ks",
        '17'=>"Telenor top up - 1000 Ks",
        '18'=>"Amazon Gift Card - $10",
        '19'=>"Amazon Gift Card - $25",
        '20'=>"Amazon Gift Card - $50",
        '21'=>"iTunes Gift Card 5 $",
        '22'=>"iTunes Gift Card 10 $",
        '23'=>"iTunes Gift Card 15 $",
        '24'=>"iTunes Gift Card 25 $",
        '25'=>"iTunes Gift Card 50 $",
        '26'=>"Google Play Gift Card 10 $",
        '27'=>"Google Play Gift Card 15 $",
        '28'=>"Google Play Gift Card 25 $",
        '29'=>"Google Play Gift Card 50 $",
        '30'=>"Steam Wallet Card - 2 $",
        '31'=>"Steam Wallet Card - 3 $",
        '32'=>"Steam Wallet Card - 5 $",
        '33'=>"Steam Wallet Card - 10 $",
        '34'=>"Steam Wallet Card - 20 $",
        '35'=>"Skype Credit Voucher 10 $",
        '36'=>"Skype Credit Voucher 25 $",
        '37'=>"MyanmarNet  Card 1000 ks",
        '38'=>"MyanmarNet  Card 3000 ks",
        '39'=>"MyanmarNet  Card 5000 ks",
        '40'=>"MyanmarNet  Card 10000 ks",
        '41'=>"Myanmar Music Store 2000 ks",
        '42'=>"Myanmar Music Store 5000 ks",
        '43'=>"BaganKeyboard Pro",
        '44'=>"Wunzinn Reader Card",
        '45'=>"Easy Point 1000 ks",
        '46'=>"Easy Point 2000 ks",
        '47'=>"Easy Point 3000 ks",
        '48'=>"Easy Point 5000 ks",

    ],

    'normaltype'=>[
        '1'=>"normal",
        '2'=>"special",
        '3'=>"onetime",
        '4'=>"onetimespecial"

    ],

    'usertype'=>[
        '1'=>"normal",
        '2'=>"VIP",


    ],

    'Art&Ebooks'=>[
        'category'=>'Art&Ebooks'
    ],
    'Videos'=>[
        'category'=>'Videos'
    ],
    'Audios'=>[
        'category'=>'Audios'
    ],
    'Other'=>[
        'category'=>'Other'
    ],
    'MyanmarSubtitles'=>[
        'category'=>'MyanmarSubtitles'
    ],

    'PrepaidCards'=>[
        'category'=>'PrepaidCards'
    ],

];