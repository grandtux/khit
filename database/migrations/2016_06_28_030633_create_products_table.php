<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type');
            $table->string('category');
            $table->string('dimen');
            $table->string('size');
            $table->integer('uploadby')->unsigned()->default(0);
            $table->foreign('uploadby')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->string('email');
            $table->integer('points');
            $table->boolean('status')->default(false);
            $table->string('media');
            $table->longText('other');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
