@extends('admin.theme')
@section('content')


    <table id="example" class="display hover table" >
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>type</th>
            <th>category</th>
            <th>points</th>
            <th>Upload by</th>
            <th>File</th>
            <th>Size</th>
            <th>Publish</th>
            <th>Delete</th>



        </tr>
        </thead>
        <tbody class="table">
        @foreach($products as $product)
            <tr>

                <td>{{ $product->id }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->type }} </td>
                <td>{{ $product->category }}</td>
                <td>{{ $product->points }}</td>
                <td>{{$product->author->name}}</td>
                <td>{{explode("/",$product->file)[2]}}</td>
                <td>{{$product->size}}</td>


                <td>
                    <a class="btn btn-info btn-xs" href="{{action("ProductController@publish", [$product->id] )}}">Publish</a>
                </td>
                <td>
                    <form action="{{route("products.destroy",$product->id)}}" method="POST" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input class="btn btn-danger" type="submit" value="Delete">
                    </form>
                </td>




            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $products->render() !!}
@endsection