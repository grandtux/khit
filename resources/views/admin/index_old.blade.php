@extends('master')
@section('content')
    @foreach($products as $product)
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-4">
                    <img src="{{asset($product->media) }}"  class="img-responsive" />


                </div>
                <div class="col-md-4">
                    {{ $product->type }} <br>
                    {{ $product->category }} <br>
                    {{ $product->points }} <br>
                    {{ $product->author->name }} <br>
                    {{ $product->size }} <br>
                </div>
                <div class="col-md-4">

                    <a class="btn btn-info btn-xs" href="{{action("ProductController@publish", [$product->id] )}}">Publish</a>
                    <a class="btn btn-info btn-xs" href="{{route("products.destroy",$product->id)}}">Delete</a>


                </div>
            </div>
            <br>
        </div>
    @endforeach
@endsection