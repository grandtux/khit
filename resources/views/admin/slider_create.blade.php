@extends('admin.theme')
@section('content')
    <div class="container">

        <h2>Index Create</h2>
        @include('errors.list')

        <form action="/slider" method="post" enctype="multipart/form-data" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <form-group enctype="multipart/form-data" >
                <label for="size">Image</label>
                <input type="file" id="pathurl" name="pathurl">
            </form-group>
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Submit</button>
            </div>
        </form>

    </div>
@endsection