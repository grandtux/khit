@extends('admin.theme')
@section('content')
    <div class="container">

        <h2>Index Create</h2>
        @include('errors.list')

        <form action="{{route("slider.update",$slider->id)}}" method="post" enctype="multipart/form-data" >
            <input type="hidden" name="_method" value="PATCH" accept-charset="UTF-18">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <form-group enctype="multipart/form-data" >
                <label for="size">Image</label>
                <input type="file" id="pathurl" name="pathurl">
            </form-group>
            <div class="form-group">
                <img src="{{asset($slider->pathurl) }}"  class="img-responsive" />
            </div>
            <div class="form-group">
                <label for="dec">Description</label>
                <textarea name='info'class="form-control" id="other">{{ old('info') }}</textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Submit</button>
            </div>
        </form>

    </div>
@endsection