@extends('admin.theme')
@section('content')
    <h2>Services admin view</h2>
    <table id="example" class="display hover table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Image</th>
            <th>Edit</th>
        </tr>
        </thead>
        <tbody class="table">
        @foreach($sliders as $slider)
            <tr>
                <td>{{ $slider->id }}</td>
                <td><img src="{{asset($slider->pathurl) }}" alt="" class="img-responsive"/></td>
                <td>
                    <a class="btn btn-info" href="{{route("slider.edit",$slider->id)}}">Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection