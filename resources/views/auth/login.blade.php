@extends('theme')
@section('content')
    <div class="container">
        <div class="row">
            <form autocomplete="off" role="form" method="POST" action="{{ url('/auth/login') }}">
                {{ csrf_field() }}
                <fieldset>
                    {{--<div class="form-group label-floating">--}}
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-account"></i>
                          </span>
                            <label class="control-label" for="ms-form-user">Email</label>
                            <input type="text" name='email' id="ms-form-user" class="form-control"> </div>
                    {{--</div>--}}
                    {{--<div class="form-group label-floating">--}}
                        <div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                          </span>
                            <label class="control-label" for="ms-form-pass">Password</label>
                            <input type="password" name='password' id="ms-form-pass" class="form-control"> </div>
                    {{--</div>--}}
                    <div class="row mt-2">
                        <div class="col-md-6">
                            <div class="form-group no-mt">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Remember Me </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-raised btn-primary pull-right">Login</button>
                        </div>
                    </div>
                </fieldset>
            </form>
            <div class="text-center">
                <h3>Login with</h3>
                <a href="/auth/facebook" class="wave-effect-light btn btn-raised btn-facebook">
                    <i class="zmdi zmdi-facebook"></i> Facebook</a>
            </div>
        </div>
    </div>




@endsection



