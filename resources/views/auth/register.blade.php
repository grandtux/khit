@extends('theme')

@section('content')
	<form role="form" method="POST" action="{{ url('/auth/register') }}">
		{{ csrf_field() }}
		<fieldset>
			{{--<div class="form-group label-floating">--}}
				<div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-account"></i>
                          </span>
					<label class="control-label" for="ms-form-user">Name</label>
					<input type="text" id="ms-form-user" class="form-control"
						   name="name" value="{{ old('name') }}" required autofocus> </div>
			{{--</div>--}}
			{{--<div class="form-group label-floating">--}}
				<div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-email"></i>
                          </span>
					<label class="control-label" for="ms-form-email">Email</label>
					<input type="email" id="ms-form-email" class="form-control"
						   name="email" value="{{ old('email') }}" required> </div>
			{{--</div>--}}
			{{--<div class="form-group label-floating">--}}
				<div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                          </span>
					<label class="control-label" for="ms-form-pass">Password</label>
					<input type="password" id="ms-form-pass" class="form-control"
						   name="password" required> </div>
			{{--</div>--}}
			{{--<div class="form-group label-floating">--}}
				<div class="input-group">
                          <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                          </span>
					<label class="control-label" for="ms-form-pass">Re-type Password</label>
					<input type="password" id="ms-form-pass" class="form-control"
						   name="password_confirmation" required> </div>
			</div>
			<button class="btn btn-raised btn-block btn-primary">Register Now</button>
			<div class="text-center">
				<h3>Register with</h3>
				<a href="/auth/facebook" class="wave-effect-light btn btn-raised btn-facebook">
					<i class="zmdi zmdi-facebook"></i> Facebook</a>
			</div>
		</fieldset>
	</form>
@endsection
