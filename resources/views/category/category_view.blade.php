@extends('master')
@section('content')
<div class="col-md-9">
    <div class="row">
        <div class="col-lg-12">
            @if(isset($tag))
                <h3 class="page-header">
                    Tag : {{$tag}}
                </h3>
            @else
                <h3 class="page-header">
                    Category : {{$cat}}
                </h3>
            @endif
        </div>
    </div>

    <div class="row">
        @foreach($products as $product)
            <div class="col-md-4 portfolio-item">

                <a href="{{url('products',$product->id)}}"><img class="img-responsive" src="{{asset($product->media) }}" alt=""></a>

                <h4><a href="{{url('products',$product->id)}}">{{ $product->name }}</a></h4>

                <div class="row">
                    <div class="col-md-6">


                        <p>
                            {{ $product->category }}

                            <br>
                            {{ $product->size }}
                            <br>
                            By:<a href="{{ url('products/user/'.$product->uploadby)}}"> {{ $product->author->name}}</a>
                        </p>
                    </div>
                    <div class="col-md-6">
                        <h4 class="home-ps">{{ $product->points }}</h4>
                        {{ $product->type }}
                    </div>
                </div>

            </div>
        @endforeach
    </div>



    <nav aria-label="..." class="text-center">
    <ul class="pagination pagination-lg">

    {!! $products->render() !!}

    </ul>
    </nav>

    <div class="row text-center">
        <div class="col-lg-12">
            <ul class="pagination">
                {!! $products->render() !!}

            </ul>
        </div>
    </div>
</div>

    <hr>

@endsection
