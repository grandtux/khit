@extends('theme')
@section('content')
    @include('info')
    <div class="container">
        <div class="row">
            @include('sidebar')


            <div class="col-md-9">
                <div class="row" id="Container">
                    @if(isset($tag))
                        <h4 align="center"><a href="javascript:void(0)">Tag : {{$tag}}</a></h4>
                    @else
                        <h4 align="center"><a href="javascript:void(0)">Category : {{$cat}}</a></h4>
                    @endif
                        {{--<h4 align="center"><a href="{{url('category/PrepaidCards')}}" id="category" name="category"--}}
                                              {{--value="PrepaidCards">Game / Gift / PrepaidCards</a></h4>--}}


                    @foreach($products as $product)
                        <div class="col-lg-4 col-md-6 col-xs-12">
                            <div class="card ms-feature">
                                <div class="card-block text-center">
                                    <a href="{{url('products',$product->id)}}">

                                        <img src="{{asset($product->media) }}" alt="" class="img-responsive center-block">
                                    </a>
                                    <h4 class="text-normal text-center">

                                        {{ str_limit($product->name,20) }}
                                    </h4>
                                    <div class="mt-2">
                      <span class="mr-2">
                          <a href="{{ url('products/user/'.$product->uploadby)}}">{{ $product->author->name }}</a>
                      </span>
                                        <span class="ms-tag ms-tag-success">
                                    {{ $product->type }}
                                </span>
                                    </div>
                                    <a href="{{url('products',$product->id)}}" class="btn btn-primary btn-sm btn-block btn-raised mt-2 no-mb">
                                        <i class="zmdi zmdi-shopping-cart-plus"></i>{{ $product->points }}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row text-center">
                    <div class="col-lg-12">
                        <ul class="pagination">
                            {!! $products->render() !!}

                        </ul>
                    </div>
                </div>
            </div>


        </div>
    </div>

@endsection