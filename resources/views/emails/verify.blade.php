<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Verify Your Email Address</h2>

<div>
    Account activation with the khitmarket.com
    Welcome '{{ $user->name }}'.
    Please follow the link below to verify your email address.<br/>

    {{ URL::to('register/verify/' . $user->activation_code) }}.<br/>

    Thank you for registering with the our Site.
    This message was generated automatically. Please do not reply.

    Have a nice day,
    khitmarket.com
</div>

</body>
</html>