<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

@section('htmlheader')
    @include('layout_admin.partials.htmlheader')
@show
@yield('header')
<body class="skin-blue sidebar-mini">
<div class="wrapper">

    @include('layout_admin.partials.mainheader')

    @include('layout_admin.partials.htmlheader')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('layout_admin.partials.contentheader')

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('layout_admin.partials.controlsidebar')

    @include('layout_admin.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
    @include('layout_admin.partials.scripts')
@show
@yield('script')
</body>
</html>
