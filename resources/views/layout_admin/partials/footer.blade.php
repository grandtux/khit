<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="#"></a><b>YokeKha</b></a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 July <a href="http://yokekha.com">yokekha.com</a></strong>
</footer>
