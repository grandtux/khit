<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ url('/') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        @if(Auth::user()->role == "admin")
            <span class="logo-mini"><b>Khit</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Khit</b>Admin Panel</span>
        @endif
        <span class="logo-mini"><b>Khit</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Khit</b> User Admin PanelAdmin</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                

                @if (Auth::guest())
                    <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                   {{--<li>--}}
                       {{--<a href="{{ url('/profile/'.Auth::id()) }}">{{ Auth::user()->name }}</a>--}}
                   {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="{{ url('/auth/logout') }}">Logout</a>--}}
                    {{--</li>--}}
                    <li class="dropdown">
                        <a href="{{ url('/register') }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} ({{ Auth::user()->points }})<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                        </ul>
                    </li>
                @endif

            </ul>
        </div>
    </nav>
</header>
