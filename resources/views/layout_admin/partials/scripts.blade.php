<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->


<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<script src="https://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="https://cdn.bootcss.com/admin-lte/2.3.5/js/app.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            "order": [[1, "desc"]]
        });
    });
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
