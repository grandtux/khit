<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">


    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">

            <!-- Optionally, you can add icons to the links -->
            <li><a href="{{ url('/products/create') }}"><i class='fa fa-link'></i>Add new Product</a></li>
            <li><a href="{{ url('/user/'.Auth::id().'/products') }}"><i class='fa fa-link'></i>My Products</a></li>
            <li><a href="{{ url('user/sales') }}"><i class='fa fa-link'></i>My Sale</a></li>
            <li><a href="{{ url('user/purchases') }}"><i class='fa fa-link'></i>My Buy</a></li>
            <li><a href="{{ url('/profile/'.Auth::id())}}"><i class='fa fa-link'></i>My Profile</a></li>
            <li><a href="{{ url('/messages')}}"><i class='fa fa-link'></i>Message</a></li>

            @if(Auth::user()->role == "admin")
                <li>
                    <a href="{{ url('/tag') }}"><i class='fa fa-link'></i>Tag List</a></li>
                </li>
                <li>
                    <a href="{{ url('/user') }}"><i class='fa fa-link'></i>User List</a></li>
                </li>
                <li>
                    <a href="{{ url('/sales') }}"><i class='fa fa-link'></i>All Sale</a></li>
                </li>
                <li>
                    <a href="{{ url('/admin') }}"><i class='fa fa-link'></i>Unpub Products</a></li>
                </li>
                <li>
                    <a href="{{ url('/published/products') }}"><i class='fa fa-link'></i>Pub Products</a></li>
                </li>
                <li>
                    <a href="{{ url('/prepaid/create') }}"><i class='fa fa-link'></i>Create Prepaid</a></li>
                </li>
                <li>
                    <a href="{{ url('/prepaid') }}"><i class='fa fa-link'></i>Prepaids</a></li>
                </li>
                <li>
                    <a href="{{ url('/slider') }}"><i class='fa fa-link'></i>SLider</a>
                </li>
                <li>
                    <a href="{{ url('/updatepoints') }}"><i class='fa fa-link'></i>Point List</a>
                </li>

                <li>
                    <a href="{{ url('/commissions/'.\Carbon\Carbon::now()->year) }}"> <i class="fa fa-link"></i>Commissions</a>
                </li>
            @endif

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
