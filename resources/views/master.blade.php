<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Marketplace For Anyone Who Wants to Buy and Sell  Digital Files">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Khit</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="images/android-desktop.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/ios-desktop.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="images/favicon.png">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css"/>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/signin.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.cyan-light_blue.min.css">
    <link href="{{ asset('style.css') }}" rel="stylesheet">
    <link href="{{ asset('bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="assets/custom.css">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=293965567660478";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
@include('par.bootnav')

<div class="container" style="margin-top: -20px;">


    <div class="container mdl-layout__content col-md-12">
        <div class="row">
            <div class="col-md-3">

                <p class="lead">
                <form action="/search" method="get" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search" name="search" id="search">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
                </p>
                <div class="list-group list-bg-color">
                    <a href="{{url('category/PrepaidCards')}}" id="category" name="category"
                       class="list-group-item" value="PrepaidCards">Game / Gift / PrepaidCards</a>
                    <a href="{{url('category/MyanmarSubtitles')}}" id="category" name="category"
                       class="list-group-item" value="MyanmarSubtitles">Myanmar Subtitles</a>
                    <a href="{{url('category/Art&Ebooks')}}" id="category" name="category"
                       value="Art&Ebooks" class="list-group-item">Art & Ebooks</a>
                    <a href="{{url('category/Videos')}}" id="category" name="category"
                       class="list-group-item" value="Videos">Videos</a>
                    <a href="{{url('category/Audios')}}" id="category" name="category"
                       value="Audios" class="list-group-item">Audios</a>
                    <a href="{{url('category/Apk')}}" id="category" name="category"
                       class="list-group-item" value="Apk">Apk</a>
                </div>
            </div>
            @include('errors.list')
            @include('partials.flash')

            @yield("content")

        </div>
    </div>
</div>
    <footer>
        <div class="row">
            <div class="col-md-12" style="padding-top: 30px">
                <p>Copyright &copy; Khit 2016.Developed by <a href="http://www.grandtux.com">GrandTux</a></p>

            </div>

        </div>
        <!-- /.row -->
    </footer>


</body>
</html>
