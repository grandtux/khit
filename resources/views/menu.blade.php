<div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
        <li class="dropdown">
            <a href="{{ url('/') }}" >Home</a>
        </li>
        <li class="dropdown">
            <a href="{{ url('/page/1') }}">About Us</a>
        </li>
        <li>
            <a href="{{ url('/page/2') }}">FAQ</a>
        </li>
        <li>
            <a href="{{ url('/page/3') }}">How to Buy</a>
        </li>
        <li>
            <a href="{{ url('/page/4') }}">User Guide</a>
        </li>
        <li>
            <a href="{{ url('/page/5') }}">Terms & Condtions</a>
        </li>
    </ul>
</div>


<!-- navbar-collapse collapse -->
<a href="javascript:void(0)" class="sb-toggle-left btn-navbar-menu">
    <i class="zmdi zmdi-menu"></i>
</a>