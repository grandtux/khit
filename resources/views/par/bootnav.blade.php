<nav class="navbar navbar-default" role="navigation">
    <div class="container" style="">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            {{--<a class="navbar-brand" href="#">Khit</a>--}}
            <a href="{{ url('/') }}" class="navbar-brand"> <img src="/upload/klogo.png" alt="" style="width: 120px;height: 50px;margin-top: -13px" class="img-responsive"></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                </li>
                <li>
                    <a href="{{ url('/page/1') }}">About Us</a>
                </li>
                <li>
                    <a href="{{ url('/page/2') }}">FAQ</a>
                </li>
                <li>
                    <a href="{{ url('/page/3') }}">How to Buy</a>
                </li>
                <li>
                    <a href="{{ url('/page/4') }}">User Guide</a>
                </li>
                <li>
                    <a href="{{ url('/page/5') }}">Terms & Condtions</a>
                </li>
            </ul>


            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li>
                        <a href="{{ url('/auth/login') }}">Login</a>
                    </li>
                    <li>
                        <a href="{{ url('/auth/register') }}">Register</a>
                    </li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">{{ Auth::user()->name }}({{ Auth::user()->points }})<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('/products/create') }}">Add new post</a>
                            </li>
                            <li>
                                <a href="{{ url('/user/'.Auth::id()).'/products' }}">My Post</a>
                            </li>
                            <li>
                                <a href="{{ url('user/sales')}}">My Sale</a>
                            </li>
                            <li>
                                <a href="{{ url('user/purchases')}}">My Buy</a>
                            </li>
                            <li>
                                <a href="{{ url('/messages')}}">Messages</a>
                            </li>
                            <li>
                                <a href="{{ url('/auth/logout') }}">Logout</a>
                            </li>
                            @if(Auth::user()->role == "admin")
                                <li>
                                    <a href="{{ url('/admin') }}">Admin Dashboard</a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
