@extends("admin.theme")
@section("content")
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Point List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Admin_ID</th>
                                <th>User_ID</th>
                                <th>User</th>
                                <th>Points</th>
                                <th>Time</th>



                            </tr>
                            </thead>
                            <tbody>
                            @foreach($points as $point)
                                <tr>

                                    <td>{{ $point->id }}</td>
                                    <td>{{ $point->admin_id }}</td>
                                    <td>{{ $point->user_id }}</td>
                                    <td>{{ $point->user->name }}</td>
                                    <td>{{ $point->points }}</td>
                                    <td>{{$point->created_at}}</td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection