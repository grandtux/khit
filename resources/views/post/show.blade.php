@extends('theme')
@section('content')
	<div class="container">
		<div class="row">
			@include('sidebar')


			<div class="col-md-9">
				<div class="row" id="Container">
					<h2 class="page-header">
						{{ $post->title }}
					</h2>
					{!!  $post->body  !!}
				</div>
			</div>


		</div>
	</div>

@endsection