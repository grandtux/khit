@extends('admin.theme')
@section('content')

    <h2>Prepaid</h2>
    @foreach($cards as $key => $value)
        <b>{{ $value->count() }}</b>
        {{ $value[0]->type  }}
        <br>
     @endforeach

    <div class="container">
        @include('errors.list')

        <form action="{{action('PrepaidController@store')}}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group">
                <label for="type">Type</label>
                <select class="form-control input-sm" name="type" id="type">
                    @if(Auth::user()->role == 'admin')
                        @foreach(config('type.prepaid') as $key=>$value)
                            <option value="{{$value}}">{{$value}}</option>
                        @endforeach
                    @endif
                </select><!-- end of Item_Drop_Down -->
            </div>

            <form-group enctype="multipart/form-data">
                <label for="image">Image</label>
                <input type="file" id="image" name="image" required="required">
            </form-group>

            <form-group enctype="multipart/form-data">
                <label for="image">Preview</label>
                <input type="file" id="sample" name="sample">
            </form-group>

            <div class="form-group">
                <label for="remark">Remark</label>
                <textarea name='remark'class="form-control" id="remark">{{ old('remark') }}</textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Submit</button>
            </div>
        </form>


    </div>
@endsection