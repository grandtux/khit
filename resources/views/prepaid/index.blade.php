@extends('admin.theme')
@section('content')
    <h3>
        Prepaid
    </h3>
    <div class="container">
        <table id="example" class="display hover table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Type</th>
                <th>Remark</th>
                <th>Created At</th>
                <th>Operation Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($cards as $card)
                <tr>
                    <td>{{ $card->id }}</td>
                    <td>{{ $card->type }}</td>
                    <td>{{ $card->remark }}</td>
                     <td>{{ $card->created_at }}</td>
                    <td>
                        <form method="POST" action="{{ route("prepaid.destroy", $card->id) }}" accept-charset="UTF-8">
                            <input name="_method" type="hidden" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-danger" value="Delete">
                        </form>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="row text-center">
        <div class="col-lg-12">
            <ul class="pagination">
                {!! $cards->render() !!}
            </ul>
        </div>
    </div>
@endsection