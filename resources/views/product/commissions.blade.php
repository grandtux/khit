@extends('admin.theme')
@section('content')
    <h3>
        Commissions
    </h3>
    <div class="container">
        <table id="commission" class="display hover table">
            <thead>
            <tr>
                <th>Limit</th>
                <th>Commission</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>{{ "Grand Total"}}</th>
                <td>{{ $commission['total'] }}</td>
            </tr>
            <tr>
                <th>{{ "January"}}</th>
                <td>{{ $commission['jan'] }}</td>
            </tr>
            <tr>
                <th>{{ "February"}}</th>
                <td>{{ $commission['feb'] }}</td>
            </tr>
            <tr>
                <th>{{ "March"}}</th>
                <td>{{ $commission['mar'] }}</td>
            </tr>
            <tr>
                <th>{{ "April"}}</th>
                <td>{{ $commission['apr'] }}</td>
            </tr>
            <tr>
                <th>{{ "May"}}</th>
                <td>{{ $commission['may'] }}</td>
            </tr>
            <tr>
                <th>{{ "June"}}</th>
                <td>{{ $commission['jun'] }}</td>
            </tr>
            <tr>
                <th>{{ "July"}}</th>
                <td>{{ $commission['july'] }}</td>
            </tr>
            <tr>
                <th>{{ "August"}}</th>
                <td>{{ $commission['aug'] }}</td>
            </tr>
            <tr>
                <th>{{ "September"}}</th>
                <td>{{ $commission['sep'] }}</td>
            </tr>
            <tr>
                <th>{{ "October"}}</th>
                <td>{{ $commission['oct'] }}</td>
            </tr>
            <tr>
                <th>{{ "November"}}</th>
                <td>{{ $commission['nov'] }}</td>
            </tr>
            <tr>
                <th>{{ "December"}}</th>
                <td>{{ $commission['dec'] }}</td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
