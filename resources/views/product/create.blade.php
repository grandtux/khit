@extends('admin.theme')
@section('content')

    <h2>Customer Product Create </h2>

    <div class="container">
        @include('errors.list')

        <form action="/products" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">


            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control"  value="{{ old('name') }}" required="required">
            </div>

            <div class="form-group">
                <label for="type">Type</label>
                <select class="form-control input-sm" name="type" id="type">
                    @if(Auth::user()->role == 'admin')
                        @foreach(config('type.type') as $key=>$value)
                            <option value="{{$value}}">{{$value}}</option>
                        @endforeach
                    @else
                        @foreach(config('type.normaltype') as $key=>$value)
                            <option value="{{$value}}">{{$value}}</option>
                        @endforeach
                    @endif



                </select><!-- end of Item_Drop_Down -->
            </div>

            <div class="form-group">
                <label for="type">category</label>
                <select class="form-control input-sm" name="category" id="category" required="required">
                    <option value="">please select</option>
                    @foreach(config('type.all') as $key=>$value)
                        <option value="{{$value}}">{{$value}}</option>
                    @endforeach
                </select><!-- end of Item_Drop_Down -->
            </div>

            <div class="form-group">
                {!! Form::label('tag_list','Tags:') !!}
                {!! Form::select('tag_list[]',$tags,null,['id'=>'tag_list','class'=>'form-control','multiple']) !!}

            </div>

            <div class="form-group">


                <label for="Points">Points</label>
                <input type="number" name="points" id="points" class="form-control" value="{{ old('points') }}"/>

            </div>


            <div class="form-group">
                <label for="dec">Description</label>
                <textarea name='other'class="form-control" id="other">{{ old('other') }}</textarea>
                <p>Please use " &#60;br&#62; " for line spacing.</p>
            </div>


            <div class="form-group">
                <label for="url">URL</label>
                <input type="text" name="url" id="url" class="form-control"  value="{{ old('url') }}" >
            </div>



            <form-group enctype="multipart/form-data">
                <label for="image">Preview Image</label>
                <input type="file" id="fileupload" name="image" required="required">
            </form-group>

            <form-group enctype="multipart/form-data">
                <label for="sample">Sample</label>
                <input type="file" id="sample" name="sample">
            </form-group>

            <form-group enctype="multipart/form-data">
                <label for="file">File</label>
                <input type="file" id="file" name="file" required="required">
            </form-group>
            <h4>Please Check <b>Category,Points,Image,File</b>.</h4>
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Submit</button>
            </div>
        </form>


    </div>

@endsection