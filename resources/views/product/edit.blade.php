@extends('admin.theme')
@section('content')

    <h2>Customer Edit </h2>

    <div class="container">
        @include('errors.list')
        <div class="col-md-9">
            <form action="{{route("products.update",$product->id)}}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PATCH" accept-charset="UTF-18">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{$product->name}}" required="required">
                </div>
                <div class="form-group">
                    <label for="type">Type</label>
                    <select class="form-control input-sm" name="type" id="type">
                        @if(Auth::user()->role == 'admin')
                            @foreach(config('type.type') as $key=>$value)
                                <option value="{{$value}}"
                                    @if($value == $product->type)
                                    selected="selected"
                                    @endif
                                >{{$value}}</option>
                        @endforeach
                         @else
                              @foreach(config('type.normaltype') as $key=>$value)
                                    <option value="{{$value}}"
                                          @if($value == $product->type)
                                          selected="selected"
                                           @endif
                                    >{{$value}}</option>
                               @endforeach
                        @endif
                    </select>
                </div>

                <div class="form-group">
                    <label for="category">category</label>
                    <select class="form-control input-sm" name="category" id="category">

                        @foreach(config('type.all') as $key=>$value)
                            <option value="{{$value}}"
                                    @if($value == $product->category)
                                    selected="selected"
                                    @endif
                            >{{$value}}</option>
                        @endforeach
                    </select><!-- end of Item_Drop_Down -->
                </div>

                <div class="form-group">
                    {!! Form::label('tag_list','Tags:') !!}
                    {!! Form::select('tag_list[]',$tags,$taglists,['id'=>'tag_list','class'=>'form-control','multiple']) !!}

                </div>


                <div class="form-group">
                    <label for="Points">Points</label>
                    <input type="number" name="points" id="points" class="form-control" value="{{$product->points}}"/>
                </div>

                <div class="form-group">
                    <label for="dec">Description</label>
                    <textarea name='other' class="form-control" id="other">{{$product->other}}</textarea>
                    <p>Please use " &#60;br&#62; " for line spacing.</p>
                </div>

                <div class="form-group">
                    <label for="url">URL</label>
                    <input type="text" name="url" id="url" class="form-control"  value="{{ $product->urls }}" >
                </div>

                <form-group enctype="multipart/form-data">
                    <label for="image">Preview Image</label>
                    <input type="file" id="image" name="image">
                </form-group>

                <form-group enctype="multipart/form-data">
                    <label for="sample">Sample</label>
                    <input type="file" id="sample" name="sample">
                </form-group>

                <form-group enctype="multipart/form-data">
                    <label for="file">File</label>
                    <input type="file" id="file" name="file" >
                </form-group>

                <img src="{{asset($product->media) }}" class="img-responsive"/>

                {{--{{asset($product->file) }}--}}
                <h4>If you edit your products,you will have wait for admin's permission again to sale.</h4>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary form-control">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection