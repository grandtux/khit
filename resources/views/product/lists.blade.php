@extends('theme')
@section('content')
    @include('info')
    <div class="container">
        @include('errors.list')
        @include('partials.flash')
        <div class="row">
            {{--@include('sidebar')--}}

            <div class="col-md-9">
                <div class="row" id="Container">
                    @for($i=0;$i<$ppcards->count();$i++)
                        <div class="col-lg-4 col-md-6 col-xs-12">
                            <div class="card ms-feature">
                                <div class="card-block text-center">
                                    <a href="{{url('products',$ppcards[$i]->id)}}">

                                        <img src="{{asset($ppcards[$i]->media) }}" alt="" class="img-responsive center-block">
                                    </a>
                                    <h4 class="text-normal text-center">
                                        {{ $ppcards[$i]->name }}
                                        {{--{{ str_limit($ppcards[$i]->name,20) }}--}}
                                    </h4>
                                    <div class="mt-2">
                      <span class="mr-2">
                          {{--<a href="{{ url('products/user/'.$ppcards[$i]->uploadby)}}">{{ $ppcards[$i]->author->name }}</a>--}}
                          Prep
                      </span>
                                        <span class="ms-tag ms-tag-success">
                                   {{ $ppcards[$i]->points }}
                                </span>
                                    </div>
                                    <a href="{{url('products',$ppcards[$i]->id)}}" class="btn btn-primary btn-sm btn-block btn-raised mt-2 no-mb">
                                        <i class="zmdi zmdi-shopping-cart-plus"></i>Claim</a>
                                </div>
                            </div>
                        </div>
                        @if($i % 3==2)
                            <div class="row" id="Container">
                                <img src="https://via.placeholder.com/1920x100.png" class="responsive">
                            </div>
                        @endif

                    @endfor
                </div>
            </div>


        </div>
    </div>

@endsection