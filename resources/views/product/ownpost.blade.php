@extends('admin.theme')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Upload By : {{ Auth::user()->name }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Point</th>
                                <th>Status</th>
                                <th>Operation Edit</th>
                                <th>Operation Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{ $product->id }}</td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->points }}</td>
                                    @if($product->status == false)
                                        <td>{{ "Unpublished" }}</td>
                                    @else
                                        <td>{{ "Published" }}</td>
                                    @endif
                                    <td>
                                        <div class="form-group">
                                            <a type="button" class="btn btn-primary"
                                               href="{{ action('ProductController@edit', [$product->id] ) }}">Edit</a>
                                        </div>
                                    </td>
                                    <td>
                                        <form method="POST" action="{{ route("products.destroy", $product->id) }}" accept-charset="UTF-8">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-danger" value="Delete">
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection