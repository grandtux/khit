@extends("admin.theme")
@section("content")

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Purchase Data table</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Seller</th>
                                <th>Product</th>
                                <th>Point</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($buys as $buy)
                                <tr>
                                    @if(isset($buy->seller) )
                                        <td>{{ $buy->seller->name}}</td>
                                    @else
                                        <td>{{ $buy->seller_id }}</td>
                                    @endif
                                    <td>{{ $buy->product_name }}</td>
                                    <td>{{ $buy->getpoints + $buy->commission }}</td>
                                    <td>{{ $buy->created_at }}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </section>


@endsection

