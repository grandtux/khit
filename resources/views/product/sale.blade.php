@extends("admin.theme")
@section("content")
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Sales Data table</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Buyer</th>
                                <th>Seller</th>
                                <th>Seller_ID</th>
                                <th>Product</th>
                                <th>Get Points</th>
                                <th>Commission</th>
                                <th>Date</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sales as $sale)
                                <tr>
                                    <td>{{ $sale->id }}</td>
                                    @if(isset($sale->buyer) )
                                        <td>{{ $sale->buyer->name}}</td>
                                    @else
                                        <td>{{ $sale->buyer_id }}</td>
                                    @endif
                                    @if(isset($sale->seller) )
                                        <td>{{ $sale->seller->name}}</td>
                                    @else
                                        <td>{{ $sale->seller_id }}</td>
                                    @endif
                                    <td>{{ $sale->seller_id }}</td>
                                    <td>{{ $sale->product_name }}</td>
                                    <td>{{ $sale->getpoints }}</td>
                                    <td>{{ $sale->commission }}</td>
                                    <td>{{ $sale->created_at }}</td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection