@extends('theme')
@section("content")


    {{--<div class="col-md-9">--}}

    {{--<div class="col-md-6" style="padding-left:20px;padding-top: 16px;">--}}
    {{--<h4 class="product-name">{{ $product->name }}</h4>--}}

    {{--<img src="{{asset($product->media) }}" class="pull-left"/>--}}
    {{--</div>--}}
    {{--<br>--}}
    {{--<div class="col-md-6" style="padding-left:0;padding-top: 16px;">--}}

    {{--<h4><label for="name">{{ $product->name }}</label></h4>--}}

    {{--<h4><label for="category">Category :</label>--}}
    {{--<label for=arbcategory>  {{ $product->category }} <br></label></h4>--}}


    {{--<h4><label for="type">Type:</label>--}}
    {{--<label for=arbtype>  {{ $product->type }} <br></label></h4>--}}


    {{--<h4><label for="points">Price :</label>--}}
    {{--<label for=arbpoints>  {{ $product->points }} <br></label></h4>--}}

    {{--@unless ($product->other == null)--}}
    {{--<div class="form-group">--}}
    {{--<p><label for=arbother>  {!!   $product->other !!} <br></label></p>--}}
    {{--</div>--}}
    {{--@endunless--}}



    {{--<h4><label for="size">Size :</label>--}}
    {{--<label for=arbsize>  {{ Khit::bytesToHuman($product->size) }} <br></label></h4>--}}

    {{--<h4><label for="author">Upload by:</label>--}}
    {{--<label for=arbauthor>  {{ $product->author->name }} </label></h4>--}}
    {{--@unless ($product->tags->isEmpty())--}}

    {{--<h4><label for="tags">Tags :</label>--}}

    {{--@foreach ($product->tags as $tag)--}}
    {{--<a href="{{action("ProductController@product_tag",[$tag->id] )}}">{{ $tag->name}}</a>{{ " |" }}--}}
    {{--@endforeach--}}

    {{--</h4>--}}
    {{--@endunless--}}

    {{--@unless ($product->sample =="" || $product->author->id == Auth::user()->id )--}}
    {{--<a class="btn btn-primary" href="{{action("PurchaseController@sample", [$product->id] )}}">Sample Download</a>--}}
    {{--@endunless--}}
    {{--@if($product->author->id == Auth::user()->id)--}}
    {{--<form action="{{route("products.edit",$product->id)}}" method="get" class="form-group">--}}
    {{--<button type="submit" class="btn btn-primary form-control">Edit</button>--}}
    {{--</form>--}}
    {{--@else--}}
    {{--@if($product->points > Auth::user()->points)--}}
    {{--<a class="btn btn-primary" href="{{action("PointController@index" )}}">Buy Points</a>--}}
    {{--@elseif($product->type == "prepaid")--}}
    {{--{!! Form::open(['action' => ['PurchaseController@prepaid', $product->id], 'method'=>'post']) !!}--}}
    {{--{!! Form::submit('Purchase',['class' => 'btn btn-primary purchase']) !!}--}}
    {{--{!! Form::close() !!}--}}
    {{--@elseif($product->type == "special" || $product->type == "onetimespecial")--}}
    {{--<a class="btn btn-primary purchase"--}}
    {{--href="{{action("PurchaseController@special", [$product->id] )}}">Purchase</a>--}}
    {{--<form action="{{action("PurchaseController@special", [$product->id] )}}" method="post" class="form-group">--}}
    {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
    {{--<button type="submit" class="btn btn-primary purchase">Purchase </button>--}}
    {{--</form>--}}
    {{--{!! Form::open(['action' => ['PurchaseController@special', $product->id], 'method'=>'post']) !!}--}}
    {{--{!! Form::submit('Purchase',['class' => 'btn btn-primary purchase']) !!}--}}
    {{--{!! Form::close() !!}--}}
    {{--@else--}}
    {{--<a class="btn btn-primary purchase"--}}
    {{--href="{{action("PurchaseController@index", [$product->id] )}}">Purchase</a>--}}
    {{--<form action="{{action("PurchaseController@index", [$product->id] )}}" method="post" class="form-group">--}}
    {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
    {{--<button type="submit" class="btn btn-primary purchase">Purchase </button>--}}
    {{--</form>--}}
    {{--{!! Form::open(['action' => ['PurchaseController@index', $product->id], 'method'=>'post']) !!}--}}
    {{--{!! Form::submit('Purchase',['class' => 'btn btn-primary purchase']) !!}--}}
    {{--{!! Form::close() !!}--}}
    {{--@endif--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--<div class="fb-share-button" data-href="{{action("ProductController@show", [$product->id] )}}" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>--}}

    {{--</div>--}}
    <div class="container">
        <div class="row">
            @include('errors.list')
            @include('partials.flash')
            <div class="col-md-6">
                <div id="carousel-product" class="ms-carousel ms-carousel-thumb carousel slide animated zoomInUp animation-delay-5" data-ride="carousel" data-interval="0">
                    <div class="card card-block">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="{{asset($product->media) }}" alt="..."> </div>
                            {{--<div class="item">--}}
                                {{--<img src="assets/img/demo/products/2.jpg" alt="..."> </div>--}}
                            {{--<div class="item">--}}
                                {{--<img src="assets/img/demo/products/3.jpg" alt="..."> </div>--}}
                            {{--<div class="item">--}}
                                {{--<img src="assets/img/demo/products/4.jpg" alt="..."> </div>--}}
                            {{--<div class="item">--}}
                                {{--<img src="assets/img/demo/products/5.jpg" alt="..."> </div>--}}
                        </div>
                    </div>
                    <!-- Indicators -->
                    {{--<ol class="carousel-indicators carousel-indicators-tumbs carousel-indicators-tumbs-outside">--}}
                        {{--<li data-target="#carousel-product" data-slide-to="0" class="active">--}}
                            {{--<img src="assets/img/demo/products/m1.png" alt=""> </li>--}}
                        {{--<li data-target="#carousel-product" data-slide-to="1">--}}
                            {{--<img src="assets/img/demo/products/m2.png" alt=""> </li>--}}
                        {{--<li data-target="#carousel-product" data-slide-to="2">--}}
                            {{--<img src="assets/img/demo/products/m3.png" alt=""> </li>--}}
                        {{--<li data-target="#carousel-product" data-slide-to="3">--}}
                            {{--<img src="assets/img/demo/products/m4.png" alt=""> </li>--}}
                        {{--<li data-target="#carousel-product" data-slide-to="4">--}}
                            {{--<img src="assets/img/demo/products/m5.png" alt=""> </li>--}}
                    {{--</ol>--}}
                </div>
            </div>
            <div class="col-md-6">
                <div class="card animated zoomInDown animation-delay-5">
                    <div class="card-block">
                        <h2>{{ $product->name }}</h2>
                        {{--<div class="mb-2 mt-4">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-sm-6">--}}

                                {{--</div>--}}
                                {{--<div class="col-sm-6 text-center">--}}
                                    {{--<h2 class="color-success no-m text-normal">{{ $product->points }}</h2>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        @unless ($product->other == null)
                        <p class="lead">{!! $product->other !!}</p>
                        @endunless
                        <ul class="list-unstyled">
                            <li>
                                <strong>Category: </strong>{{ $product->category }}
                            </li>

                            <li class="mb-2">
                                <strong>Type: </strong>
                                <span class="ms-tag ms-tag-success">{{ $product->type }}</span>
                            </li>
                            @unless ($product->tags->isEmpty())
                            <li>
                                <strong>Tags: </strong>
                                @foreach ($product->tags as $tag)
                                    <a href="{{action("ProductController@product_tag",[$tag->id] )}}">{{ $tag->name}}</a>{{ " |" }}
                                @endforeach
                            </li>
                            @endunless
                            <li>
                                <strong>Points : </strong>
                                <span class="color-warning">{{ $product->points }}</span>
                            </li>
                            <li>
                                <strong>Size: </strong>{{ Khit::bytesToHuman($product->size) }}
                            </li>
                            <li>
                                <strong>Sell By: </strong>{{ $product->author->name }}
                            </li>
                        </ul>
                        @unless ($product->sample =="" || $product->author->id == Auth::user()->id )
                            <a class="btn btn-primary btn-block btn-raised mt-2 no-mb" href="{{action("PurchaseController@sample", [$product->id] )}}">
                                <i class="zmdi zmdi-case-download"></i>Sample Download</a>
                        @endunless
                        {{--<a href="javascript:void(0)" class="btn btn-primary btn-block btn-raised mt-2 no-mb">--}}
                            {{--<i class="zmdi zmdi-shopping-cart-plus"></i> Add to Cart</a>--}}
                        @if($product->author->id == Auth::user()->id)
                            <form action="{{route("products.edit",$product->id)}}" method="get" class="form-group">
                                <button type="submit" class="btn btn-primary btn-block btn-raised mt-2 no-mb">Edit</button>
                            </form>
                        @else
                            {{--@if($product->points > Auth::user()->points)--}}
                                {{--<a class="btn btn-primary btn-block btn-raised mt-2 no-mb" href="{{action("PointController@index" )}}">--}}
                                    {{--<i class="zmdi zmdi-shopping-cart-plus"></i>Buy Points</a>--}}
                            @if($product->type == "special" || $product->type == "onetimespecial")
                                {!! Form::open(['action' => ['PurchaseController@special', $product->id], 'method'=>'post']) !!}
                                {!! Form::submit('Purchase',['class' => 'btn btn-primary btn-block btn-raised mt-2 no-mb']) !!}
                                {!! Form::close() !!}
                            @else
                                {!! Form::open(['action' => ['PurchaseController@index', $product->id], 'method'=>'post']) !!}
                                {!! Form::submit('Purchase',['class' => 'btn btn-primary btn-block btn-raised mt-2 no-mb']) !!}
                                {!! Form::close() !!}
                            @endif

                            <a href="{{ $product->urls }}" class = 'btn btn-primary btn-block btn-raised mt-2 no-mb' target="_blank">Link</a>
                        @endif
                    </div>
                </div>
                {{--<div class="card card-success animated fadeInUp animation-delay-10">--}}
                    {{--<div class="card-block text-center">--}}
                        {{--<i class="fa fa-5x mr-2 fa-cc-paypal" aria-hidden="true"></i>--}}
                        {{--<i class="fa fa-5x mr-2 fa-cc-visa" aria-hidden="true"></i>--}}
                        {{--<i class="fa fa-5x mr-2 fa-cc-mastercard" aria-hidden="true"></i>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
        {{--<h2 class="mt-4 mb-4 right-line">Related Products</h2>--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-4">--}}
                {{--<div class="card ms-feature wow zoomInUp animation-delay-3">--}}
                    {{--<div class="card-block text-center">--}}
                        {{--<a href="javascript:void(0)">--}}
                            {{--<img src="assets/img/demo/products/surfaceBook.png" alt="" class="img-responsive center-block">--}}
                        {{--</a>--}}
                        {{--<h4 class="text-normal text-center">Microsoft Surface Book</h4>--}}
                        {{--<p>Quibusdam aperiam tempora ut blanditiis cumque ab pariatur.</p>--}}
                        {{--<div class="mt-2">--}}
                  {{--<span class="mr-2">--}}
                    {{--<i class="zmdi zmdi-star color-warning"></i>--}}
                    {{--<i class="zmdi zmdi-star color-warning"></i>--}}
                    {{--<i class="zmdi zmdi-star color-warning"></i>--}}
                    {{--<i class="zmdi zmdi-star color-warning"></i>--}}
                    {{--<i class="zmdi zmdi-star"></i>--}}
                  {{--</span>--}}
                            {{--<span class="ms-tag ms-tag-success">$ 2499.25</span>--}}
                        {{--</div>--}}
                        {{--<a href="javascript:void(0)" class="btn btn-primary btn-sm btn-block btn-raised mt-2 no-mb">--}}
                            {{--<i class="zmdi zmdi-shopping-cart-plus"></i> Add to Cart</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4">--}}
                {{--<div class="card ms-feature wow zoomInUp animation-delay-5">--}}
                    {{--<div class="card-block text-center">--}}
                        {{--<a href="javascript:void(0)">--}}
                            {{--<img src="assets/img/demo/products/ipad.png" alt="" class="img-responsive center-block">--}}
                        {{--</a>--}}
                        {{--<h4 class="text-normal text-center">iPad Pro</h4>--}}
                        {{--<p>Quibusdam aperiam tempora ut blanditiis cumque ab pariatur.</p>--}}
                        {{--<div class="mt-2">--}}
                  {{--<span class="mr-2">--}}
                    {{--<i class="zmdi zmdi-star color-warning"></i>--}}
                    {{--<i class="zmdi zmdi-star color-warning"></i>--}}
                    {{--<i class="zmdi zmdi-star color-warning"></i>--}}
                    {{--<i class="zmdi zmdi-star color-warning"></i>--}}
                    {{--<i class="zmdi zmdi-star color-warning"></i>--}}
                  {{--</span>--}}
                            {{--<span class="ms-tag ms-tag-success">$ 999.99</span>--}}
                        {{--</div>--}}
                        {{--<a href="javascript:void(0)" class="btn btn-primary btn-sm btn-block btn-raised mt-2 no-mb">--}}
                            {{--<i class="zmdi zmdi-shopping-cart-plus"></i> Add to Cart</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4">--}}
                {{--<div class="card ms-feature wow zoomInUp animation-delay-7">--}}
                    {{--<div class="card-block text-center">--}}
                        {{--<a href="javascript:void(0)">--}}
                            {{--<img src="assets/img/demo/products/galaxyTab.png" alt="" class="img-responsive center-block">--}}
                        {{--</a>--}}
                        {{--<h4 class="text-normal text-center">Galaxy Tab S2</h4>--}}
                        {{--<p>Quibusdam aperiam tempora ut blanditiis cumque ab pariatur.</p>--}}
                        {{--<div class="mt-2">--}}
                  {{--<span class="mr-2">--}}
                    {{--<i class="zmdi zmdi-star color-warning"></i>--}}
                    {{--<i class="zmdi zmdi-star color-warning"></i>--}}
                    {{--<i class="zmdi zmdi-star color-warning"></i>--}}
                    {{--<i class="zmdi zmdi-star color-warning"></i>--}}
                    {{--<i class="zmdi zmdi-star"></i>--}}
                  {{--</span>--}}
                            {{--<span class="ms-tag ms-tag-success">$ 538.99</span>--}}
                        {{--</div>--}}
                        {{--<a href="javascript:void(0)" class="btn btn-primary btn-sm btn-block btn-raised mt-2 no-mb">--}}
                            {{--<i class="zmdi zmdi-shopping-cart-plus"></i> Add to Cart</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>

        <script>
            $(".purchase").click(function(){
                if(confirm("Will you buy this item?"))
                {
                    $(".purchase").attr("hidden",true);
                    return true;
                }
                else{
                    return false;
                }
            });
        </script>




@endsection