@extends('master')
@section('content')
    <div class="contain">
        <div class="col-md-9">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">
                        Sell By : {{ $user->name }}

                    </h3>
                </div>
            </div>

            <div class="row">
                @foreach($products as $product)
                    <div class="col-md-4 portfolio-item">

                        <a href="{{url('products',$product->id)}}"><img class="img-responsive" src="{{asset($product->media) }}" alt=""></a>

                        <h4><a href="{{url('products',$product->id)}}">{{ $product->name }}</a></h4>

                        <div class="row">
                            <div class="col-md-6">


                                <p>
                                    {{ $product->category }}

                                    <br>
                                    {{ $product->size }}
                                    <br>
                                    By:<a href="{{ url('products/user/'.$product->uploadby)}}"> {{ $product->author->name}}</a>
                                </p>
                            </div>
                            <div class="col-md-6">
                                {{ $product->points }}
                                <br>
                                {{ $product->type }}
                            </div>
                        </div>


                    </div>
                @endforeach
            </div>

            <div class="row text-center">
                <div class="col-lg-12">
                    <ul class="pagination">
                        {!! $products->render() !!}
                    </ul>
                </div>
            </div>

            <hr>
        </div>
    </div>



@endsection