@extends('theme')
@section('content')
    @include('info')
    <div class="container">
        @include('errors.list')
        @include('partials.flash')
        <div class="row">
            @include('sidebar')

            {{--<div class="col-md-9">--}}
            {{--<div class="row carousel-holder">--}}

            {{--<div class="col-md-12 hidden-xs hidden-sm">--}}
            {{--<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">--}}
            {{--<ol class="carousel-indicators">--}}
            {{--<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>--}}
            {{--<li data-target="#carousel-example-generic" data-slide-to="1"></li>--}}
            {{--<li data-target="#carousel-example-generic" data-slide-to="2"></li>--}}
            {{--</ol>--}}

            {{--<div class="carousel-inner">--}}

            {{--<div class="item active">--}}
            {{--<img class="slide-image" src="{{$sliders[0]->pathurl}}" alt="">--}}
            {{--</div>--}}
            {{--<div class="item">--}}
            {{--<img class="slide-image" src="{{$sliders[1]->pathurl}}" alt="">--}}
            {{--</div>--}}

            {{--<div class="item">--}}
            {{--<img class="slide-image" src="{{$sliders[2]->pathurl}}" alt="">--}}
            {{--</div>--}}

            {{--</div>--}}
            {{--<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">--}}
            {{--<span class="glyphicon glyphicon-chevron-left"></span>--}}
            {{--</a>--}}
            {{--<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">--}}
            {{--<span class="glyphicon glyphicon-chevron-right"></span>--}}
            {{--</a>--}}
            {{--</div>--}}
            {{--</div>--}}

            {{--</div>--}}


            {{--<div class="row">--}}

            {{--<div class="row">--}}

            {{--<h4><a href="{{url('category/PrepaidCards')}}" id="category" name="category"--}}
            {{--value="PrepaidCards">Game / Gift / PrepaidCards</a></h4>--}}
            {{--@foreach($ppcards as $ppcard)--}}
            {{--<div class="col-sm-4 col-lg-4 col-md-4">--}}
            {{--<div class="thumbnail">--}}
            {{--<a href="{{url('products',$ppcard->id)}}"><img src="{{asset($ppcard->media) }}" alt=""></a>--}}
            {{--<div class="">--}}
            {{--<h4 class="pull-right home-ps">{{ $ppcard->points }} </h4>--}}
            {{--<a class="home" href="{{url('products',$ppcard->id)}}">{{ $ppcard->name }}</a> <br>--}}
            {{--{{ $ppcard->type }} <br>--}}
            {{--{{ $ppcard->category }} <br>--}}
            {{--By:<a class="home" href="{{ url('products/user/'.$ppcard->uploadby)}}">  {{ $ppcard->author->name}}</a>--}}

            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--@endforeach--}}
            {{--</div>--}}

            {{--<div class="row">--}}
            {{--<h4><a href="{{url('category/MyanmarSubtitles')}}" id="category" name="category"--}}
            {{--value="MyanmarSubtitles">Myanmar Subtitles</a></h4>--}}
            {{--@foreach($msubs as $msub)--}}
            {{--<div class="col-sm-4 col-lg-4 col-md-4">--}}
            {{--<div class="thumbnail">--}}
            {{--<a href="{{url('products',$msub->id)}}"><img src="{{asset($msub->media) }}" alt=""></a>--}}
            {{--<div class="">--}}
            {{--<h4 class="pull-right home-ps">{{ $msub->points }} </h4>--}}
            {{--<a class="home" href="{{url('products',$msub->id)}}">{{ $msub->name }}</a> <br>--}}
            {{--{{ $msub->type }} <br>--}}
            {{--{{ $msub->category }} <br>--}}
            {{--By:<a class="home" href="{{ url('products/user/'.$msub->uploadby)}}">  {{ $msub->author->name}}</a>--}}

            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--@endforeach--}}
            {{--</div>--}}

            {{--<div class="row">--}}


            {{--<h4><a href="{{url('category/Art&Ebooks')}}" id="category" name="category"--}}
            {{--value="Art&Ebooks">Art & Ebooks</a></h4>--}}
            {{--@foreach($ebooks as $ebook)--}}
            {{--<div class="col-sm-4 col-lg-4 col-md-4">--}}
            {{--<div class="thumbnail">--}}
            {{--<a href="{{url('products',$ebook->id)}}"><img src="{{asset($ebook->media) }}" alt=""></a>--}}
            {{--<div class="">--}}
            {{--<h4 class="pull-right home-ps">{{ $ebook->points }} </h4>--}}

            {{--<a class="home" href="{{url('products',$ebook->id)}}">{{ $ebook->name }}</a> <br>--}}
            {{--{{ $ebook->type }} <br>--}}
            {{--{{ $ebook->category }} <br>--}}
            {{--By:<a  class="home" href="{{ url('products/user/'.$ebook->uploadby)}}">  {{ $ebook->author->name}}</a>--}}

            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--@endforeach--}}
            {{--</div>--}}

            {{--<div class="row">--}}
            {{--<h4><a href="{{url('category/Videos')}}" id="category" name="category"--}}
            {{--value="Videos">Videos</a></h4>--}}
            {{--@foreach($mmovies as $mmovie)--}}
            {{--<div class="col-sm-4 col-lg-4 col-md-4">--}}
            {{--<div class="thumbnail">--}}
            {{--<a href="{{url('products',$mmovie->id)}}"><img src="{{asset($mmovie->media) }}" alt=""></a>--}}
            {{--<div class="">--}}
            {{--<h4 class="pull-right home-ps">{{ $mmovie->points }} </h4>--}}

            {{--<a class="home" href="{{url('products',$mmovie->id)}}">{{ $mmovie->name }}</a> <br>--}}
            {{--{{ $mmovie->type }} <br>--}}
            {{--{{ $mmovie->category }} <br>--}}
            {{--By:<a class="home" href="{{ url('products/user/'.$mmovie->uploadby)}}">  {{ $mmovie->author->name}}</a>--}}

            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--@endforeach--}}
            {{--</div>--}}

            {{--<div class="row">--}}
            {{--<h4><a href="{{url('category/Audios')}}" id="category" name="category"--}}
            {{--value="Audios">Audios</a></h4>--}}
            {{--@foreach($mmusics as $mmusic)--}}
            {{--<div class="col-sm-4 col-lg-4 col-md-4">--}}
            {{--<div class="thumbnail">--}}
            {{--<a href="{{url('products',$mmusic->id)}}"><img src="{{asset($mmusic->media) }}" alt=""></a>--}}
            {{--<div class="">--}}
            {{--<h4 class="pull-right home-ps">{{ $mmusic->points }} </h4>--}}
            {{--<a class="home" href="{{url('products',$mmusic->id)}}">{{ $mmusic->name }}</a> <br>--}}
            {{--{{ $mmusic->type }} <br>--}}
            {{--{{ $mmusic->category }} <br>--}}
            {{--By:<a class="home" href="{{ url('products/user/'.$mmusic->uploadby)}}">  {{ $mmusic->author->name}}</a>--}}

            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--@endforeach--}}
            {{--</div>--}}
            {{--<div class="row">--}}
            {{--<h4><a href="{{url('category/Apk')}}" id="category" name="category"--}}
            {{--value="Apk">Apk</a></h4>--}}
            {{--@foreach($techs as $tech)--}}
            {{--<div class="col-sm-4 col-lg-4 col-md-4">--}}
            {{--<div class="thumbnail">--}}
            {{--<a href="{{url('products',$tech->id)}}"><img src="{{asset($tech->media) }}" alt=""></a>--}}
            {{--<div class="">--}}
            {{--<h4 class="pull-right home-ps">{{ $tech->points }} </h4>--}}
            {{--<a class="home" href="{{url('products',$tech->id)}}">{{ $tech->name }}</a> <br>--}}
            {{--{{ $tech->type }} <br>--}}
            {{--{{ $tech->category }} <br>--}}
            {{--By:<a class="home" href="{{ url('products/user/'.$tech->uploadby)}}">  {{ $tech->author->name}}</a>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--@endforeach--}}
            {{--</div>--}}



            {{--</div>--}}
            {{--</div>--}}

            <div class="col-md-9">
                <div class="row" id="Container">
                    <h3 align="center"><a href="{{url('category/PrepaidCards')}}" id="category" name="category"
                                          value="PrepaidCards">Game / Gift / PrepaidCards</a></h3>
                    @foreach($ppcards as $ppcard)
                        <div class="col-lg-4 col-md-6 col-xs-12">
                            <div class="card ms-feature">
                                <div class="card-block text-center">
                                    <a href="{{url('products',$ppcard->id)}}">

                                        <img src="{{asset($ppcard->media) }}" alt="" class="img-responsive center-block">
                                    </a>
                                    <h4 class="text-normal text-center">

                                        {{ str_limit($ppcard->name,20) }}
                                    </h4>
                                    <div class="mt-2">
                      <span class="mr-2">
                          <a href="{{ url('products/user/'.$ppcard->uploadby)}}">{{ $ppcard->author->name }}</a>
                      </span>
                                        <span class="ms-tag ms-tag-success">
                                    {{ $ppcard->type }}
                                </span>
                                    </div>
                                    <a href="{{url('products',$ppcard->id)}}" class="btn btn-primary btn-sm btn-block btn-raised mt-2 no-mb">
                                        <i class="zmdi zmdi-shopping-cart-plus"></i>{{ $ppcard->points }}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <h3 align="center"><a href="{{url('category/MyanmarSubtitles')}}" id="category" name="category"
                                          value="MyanmarSubtitles">Myanmar Subtitles</a></h3>
                    @foreach($msubs as $msub)
                        <div class="col-lg-4 col-md-6 col-xs-12">
                            <div class="card ms-feature">
                                <div class="card-block text-center">
                                    <a href="{{url('products',$msub->id)}}">

                                        <img src="{{asset($msub->media) }}" alt="" class="img-responsive center-block">
                                    </a>
                                    <h4 class="text-normal text-center">

                                        {{ str_limit($msub->name,20) }}
                                    </h4>
                                    <div class="mt-2">
                      <span class="mr-2">
                          <a href="{{ url('products/user/'.$msub->uploadby)}}">{{ $msub->author->name }}</a>
                      </span>
                                        <span class="ms-tag ms-tag-success">
                                    {{ $msub->type }}
                                </span>
                                    </div>
                                    <a href="{{url('products',$msub->id)}}" class="btn btn-primary btn-sm btn-block btn-raised mt-2 no-mb">
                                        <i class="zmdi zmdi-shopping-cart-plus"></i>{{ $msub->points }}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <h3 align="center"><a href="{{url('category/Art&Ebooks')}}" id="category" name="category"
                                          value="Art&Ebooks">Art & Ebooks</a></h3>
                    @foreach($ebooks as $ebook)
                        <div class="col-lg-4 col-md-6 col-xs-12">
                            <div class="card ms-feature">
                                <div class="card-block text-center">
                                    <a href="{{url('products',$ebook->id)}}">

                                        <img src="{{asset($ebook->media) }}" alt="" class="img-responsive center-block">
                                    </a>
                                    <h4 class="text-normal text-center">

                                        {{ str_limit($ebook->name,20) }}
                                    </h4>
                                    <div class="mt-2">
                      <span class="mr-2">
                          <a href="{{ url('products/user/'.$ebook->uploadby)}}">{{ $ebook->author->name }}</a>
                      </span>
                                        <span class="ms-tag ms-tag-success">
                                    {{ $ebook->type }}
                                </span>
                                    </div>
                                    <a href="{{url('products',$ebook->id)}}" class="btn btn-primary btn-sm btn-block btn-raised mt-2 no-mb">
                                        <i class="zmdi zmdi-shopping-cart-plus"></i>{{ $ebook->points }}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <h3 align="center"><a href="{{url('category/Videos')}}" id="category" name="category"
                                          value="Videos">Videos</a></h3>
                    @foreach($mmovies as $mmovie)
                        <div class="col-lg-4 col-md-6 col-xs-12">
                            <div class="card ms-feature">
                                <div class="card-block text-center">
                                    <a href="{{url('products',$mmovie->id)}}">

                                        <img src="{{asset($mmovie->media) }}" alt="" class="img-responsive center-block">
                                    </a>
                                    <h4 class="text-normal text-center">

                                        {{ str_limit($mmovie->name,20) }}
                                    </h4>
                                    <div class="mt-2">
                      <span class="mr-2">
                          <a href="{{ url('products/user/'.$mmovie->uploadby)}}">{{ $mmovie->author->name }}</a>
                      </span>
                                        <span class="ms-tag ms-tag-success">
                                    {{ $mmovie->type }}
                                </span>
                                    </div>
                                    <a href="{{url('products',$mmovie->id)}}" class="btn btn-primary btn-sm btn-block btn-raised mt-2 no-mb">
                                        <i class="zmdi zmdi-shopping-cart-plus"></i>{{ $mmovie->points }}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <h3 align="center"><a href="{{url('category/Audios')}}" id="category" name="category"
                                          value="Audios">Audios</a></h3>
                    @foreach($mmusics as $mmusic)
                        <div class="col-lg-4 col-md-6 col-xs-12">
                            <div class="card ms-feature">
                                <div class="card-block text-center">
                                    <a href="{{url('products',$mmusic->id)}}">

                                        <img src="{{asset($mmusic->media) }}" alt="" class="img-responsive center-block">
                                    </a>
                                    <h4 class="text-normal text-center">

                                        {{ str_limit($mmusic->name,20) }}
                                    </h4>
                                    <div class="mt-2">
                      <span class="mr-2">
                          <a href="{{ url('products/user/'.$mmusic->uploadby)}}">{{ $mmusic->author->name }}</a>
                      </span>
                                        <span class="ms-tag ms-tag-success">
                                    {{ $mmusic->type }}
                                </span>
                                    </div>
                                    <a href="{{url('products',$mmusic->id)}}" class="btn btn-primary btn-sm btn-block btn-raised mt-2 no-mb">
                                        <i class="zmdi zmdi-shopping-cart-plus"></i>{{ $mmusic->points }}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <h3 align="center"><a href="{{url('category/Other')}}" id="category" name="category"
                                          value="Other">Other</a></h3>
                    @foreach($techs as $tech)
                        <div class="col-lg-4 col-md-6 col-xs-12">
                            <div class="card ms-feature">
                                <div class="card-block text-center">
                                    <a href="{{url('products',$tech->id)}}">

                                        <img src="{{asset($tech->media) }}" alt="" class="img-responsive center-block">
                                    </a>
                                    <h4 class="text-normal text-center">

                                        {{ str_limit($tech->name,20) }}
                                    </h4>
                                    <div class="mt-2">
                      <span class="mr-2">
                          <a href="{{ url('products/user/'.$tech->uploadby)}}">{{ $tech->author->name }}</a>
                      </span>
                                        <span class="ms-tag ms-tag-success">
                                    {{ $tech->type }}
                                </span>
                                    </div>
                                    <a href="{{url('products',$tech->id)}}" class="btn btn-primary btn-sm btn-block btn-raised mt-2 no-mb">
                                        <i class="zmdi zmdi-shopping-cart-plus"></i>{{ $tech->points }}</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>


        </div>
    </div>

@endsection