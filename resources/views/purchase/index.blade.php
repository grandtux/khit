@extends('theme')
@section('content')
    @include('info')
    <div class="container">
        <div class="row">
            @include('sidebar')


            <div class="col-md-9">
                <div class="row" id="Container">
                    <h4 align="center"><a href="#">{{ $title }} </a></h4>
                    {{--<h4 align="center"><a href="{{url('category/PrepaidCards')}}" id="category" name="category"--}}
                    {{--value="PrepaidCards">Game / Gift / PrepaidCards</a></h4>--}}


                    @foreach($purchases as $purchase)
                        @if(isset($purchase->product) )
                            @unless($purchase->product->type =="prepaid")
                                <div class="col-lg-4 col-md-6 col-xs-12">
                                    <div class="card ms-feature">
                                        <div class="card-block text-center">
                                            <a href="{{url('products',$purchase->product->id)}}">

                                                <img src="{{asset($purchase->product->media) }}" alt="" class="img-responsive center-block">
                                            </a>
                                            <h4 class="text-normal text-center">

                                                {{ str_limit($purchase->product->name,20) }}
                                            </h4>
                                            <div class="mt-2">
                                                <span class="mr-2">
                                                    <a href="{{ url('products/user/'.$purchase->product->uploadby)}}">{{ $purchase->product->author->name }}</a>
                                                </span>
                                                <span class="ms-tag ms-tag-success">
                                                    {{ $purchase->product->type }}
                                                </span>
                                            </div>
                                            <a href="{{url('products',$purchase->product->id)}}" class="btn btn-primary btn-sm btn-block btn-raised mt-2 no-mb">
                                                <i class="zmdi zmdi-shopping-cart-plus"></i>{{ $purchase->product->points }}</a>
                                        </div>
                                    </div>
                                </div>
                            @endunless
                        @endif
                    @endforeach

                    <section class="content">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title">Prepaid Data table</h3>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <table id="example1" class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>Seller</th>
                                                <th>Product</th>
                                                <th>Point</th>
                                                <th>Date</th>
                                                <th>Download</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($prepaideds as $prepaided)
                                                <tr>
                                                    @if(isset($prepaided->product->author) )
                                                        <td>{{ $prepaided->product->author->name}}</td>
                                                    @else
                                                        <td></td>
                                                    @endif
                                                    <td>{{ $prepaided->product->name }}</td>
                                                    <td>{{ $prepaided->product->points }}</td>
                                                    <td>{{ $prepaided->created_at }}</td>
                                                    <td><a href="{{action("PurchaseController@prepaided", [$prepaided->id] )}}">
                                                            Download</a></td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </section>


                </div>
                {{--<div class="row text-center">--}}
                {{--<div class="col-lg-12">--}}
                {{--<ul class="pagination">--}}
                {{--{!! $products->render() !!}--}}

                {{--</ul>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>


        </div>
    </div>

@endsection