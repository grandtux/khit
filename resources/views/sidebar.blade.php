<div class="col-md-3">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Category</h3>
        </div>
        <div class="card-block">
            <form class="form-horizontal" id="Filters">
                {{--<h4 class="mb-1 no-mt">Category</h4>--}}
                <fieldset>
                    <div class="form-group no-mt">
                        <div class="checkbox ml-1">
                            {{--<label>--}}
                            {{--<input type="checkbox" value=".smartphone"> Smartphones </label>--}}
                            <a href="{{url('category/PrepaidCards')}}" id="category" name="category"
                               value="PrepaidCards">Game / Gift / PrepaidCards</a>
                        </div>
                        <div class="checkbox  ml-1">
                            {{--<label>--}}
                                {{--<input type="checkbox" value=".tablet"> Tablets </label>--}}
                            <a href="{{url('category/MyanmarSubtitles')}}" id="category" name="category"
                               value="MyanmarSubtitles">Myanmar Subtitles</a>
                        </div>
                        <div class="checkbox  ml-1">
                            {{--<label>--}}
                                {{--<input type="checkbox" value=".laptop"> Laptops </label>--}}
                            <a href="{{url('category/Art&Ebooks')}}" id="category" name="category"
                               value="Art&Ebooks">Art & Ebooks</a>
                        </div>
                        <div class="checkbox  ml-1">
                            {{--<label>--}}
                                {{--<input type="checkbox" value=".accessory"> Accesories </label>--}}
                            <a href="{{url('category/Videos')}}" id="category" name="category"
                               value="Videos">Videos</a>
                        </div>
                        <div class="checkbox  ml-1">
                            {{--<label>--}}
                            {{--<input type="checkbox" value=".accessory"> Accesories </label>--}}
                            <a href="{{url('category/Audios')}}" id="category" name="category"
                               value="Audios">Audios</a>
                        </div>
                        <div class="checkbox  ml-1">
                            {{--<label>--}}
                            {{--<input type="checkbox" value=".accessory"> Accesories </label>--}}
                            <a href="{{url('category/Other')}}" id="category" name="category"
                               value="Other">Other</a>
                        </div>
                    </div>
                </fieldset>

            </form>
            {{--<form class="form-horizontal">--}}
                {{--<h4>Sort by</h4>--}}
                {{--<select id="SortSelect" class="form-control selectpicker">--}}
                    {{--<option value="random">Popular</option>--}}
                    {{--<option value="price:asc">Price ASC</option>--}}
                    {{--<option value="price:desc">Price DESC</option>--}}
                    {{--<option value="date:asc">Release ASC</option>--}}
                    {{--<option value="date:desc">Release DESC</option>--}}
                {{--</select>--}}

            {{--</form>--}}
        </div>
    </div>

    {!! Ads::show('responsive') !!}

</div>