@extends('theme')
@section('style')
    <style>
        icon {
            border: solid black;
            border-width: 0 3px 3px 0;
            display: inline-block;
            padding: 3px;
        }

        .right {
            transform: rotate(-45deg);
            -webkit-transform: rotate(-45deg);
        }

        .left {
            transform: rotate(135deg);
            -webkit-transform: rotate(135deg);
        }

        .up {
            transform: rotate(-135deg);
            -webkit-transform: rotate(-135deg);
        }

        .down {
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
        }

        #canvasContainer {
            background-image: url(/assets/img/basic_wheel_background.png);
            background-repeat: no-repeat;   /* Ensure that background does not repeat */
            background-position: center;    /* Ensure image is centred */
            width: 400px;                   /* Width and height should at least be the same as the canvas */
            height: 400px;
        }
    </style>
@endsection
@section("content")

    <body>
    <div align="center">
        {{--<h1>Winwheel.js example wheel - wheel of fortune</h1>--}}
        {{--<p>Here is an example of a code-drawn Wheel of Fortune looking wheel which spins to a stop with the prize won alerted to the user.</p>--}}
        {{--<br />--}}
        {{--<p>--}}
        {{--With some additional coding it could be made so that the prize won is added to a total after each spin rather than--}}
        {{--just alerting the prize to make a proper wheel of fortune like game.--}}
        {{--</p>--}}
        {{--<br />--}}
        {{--<p>Choose a power setting then press the Spin button. You will be alerted to the prize won when the spinning stops.</p>--}}
        <br />
        {{--<table cellpadding="0" cellspacing="0" border="0">--}}
            {{--<tr>--}}
                {{--<td>--}}
                    {{--<div class="power_controls">--}}
                        {{--<br />--}}
                        {{--<br />--}}
                        {{--<table class="power" cellpadding="10" cellspacing="0">--}}
                            {{--<tr>--}}
                                {{--<th align="center">Power</th>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td width="78" align="center" id="pw3" onClick="powerSelected(3);">High</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td align="center" id="pw2" onClick="powerSelected(2);">Med</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td align="center" id="pw1" onClick="powerSelected(1);">Low</td>--}}
                            {{--</tr>--}}
                        {{--</table>--}}
                        {{--<br />--}}
                        {{--<img id="spin_button" src="spin_off.png" alt="Spin" onClick="startSpin();" />--}}

                        {{--5454--}}
                    {{--</div>--}}
                {{--</td>--}}
                {{--<td width="438" height="582" class="the_wheel" align="center" valign="center">--}}
                {{--<div class="container">--}}
                    {{--<canvas id="canvas" width="360" height="360">--}}
                        {{--<p style="{color: white}" align="center">Sorry, your browser doesn't support canvas. Please try another.</p>--}}
                    {{--</canvas>--}}
                {{--</div>--}}
                {{--<div class="container">--}}
                    {{--<div class="row">--}}
                        {{----}}
                    {{--</div>--}}
                    {{--<img id="spin_button" src="spin_off.png" alt="Spin" onClick="startSpin();" />--}}
                {{--</div>--}}
                {{--</td>--}}
            {{--</tr>--}}
        {{--</table>--}}
        <div id="canvasContainer">
            <canvas id="canvas" width="400" height="400">
                Canvas not supported, please user another browser.
            </canvas>
        </div>
        <div class="container">
            <input type="hidden" id="pw3" onClick="powerSelected(3);">
            <input type="hidden" id="pw2" onClick="powerSelected(2);">
            <input type="hidden" id="pw1" onClick="powerSelected(1);">
            <img id="spin_button" src="spin_on.png" alt="Spin" onClick="startSpin();" />
        </div>

        @endsection

        @section('script')
            <script>
                // Create new wheel object specifying the parameters at creation time.
                        {{--var stopAngle = "{{ $random_degree }}";--}}

                let user = "{{ \Auth::id() }}";
                let theWheel = new Winwheel({
                    'outerRadius'     : 146,        // Set outer radius so wheel fits inside the background.
                    'innerRadius'     : 50,         // Make wheel hollow so segments don't go all way to center.
                    'textFontSize'    : 18,         // Set default font size for the segments.
                    'textOrientation' : 'curved', // Make text vertial so goes down from the outside of wheel.
                    'textAlignment'   : 'outer',    // Align text to outside of wheel.
                    'numSegments'     : 18,         // Specify number of segments.
                    'segments'        :             // Define segments including colour and text.
                        [                               // font size and test colour overridden on backrupt segments.
                            {'fillStyle' : '#38cfad', 'text' : '1'},
                            {'fillStyle' : '#ffd33d', 'text' : 'SORRY','textFontSize' : 12, 'textFillStyle' : '#ffffff'},
                            {'fillStyle' : '#2265d5', 'text' : '5'},
                            {'fillStyle' : '#38cfad', 'text' : '1'},
                            {'fillStyle' : '#ab4ecb', 'text' : '10'},
                            {'fillStyle' : '#ef4066', 'text' : '25'},
                            {'fillStyle' : '#ff725b', 'text' : '50'},
                            {'fillStyle' : '#38cfad', 'text' : '1'},
                            {'fillStyle' : '#2265d5', 'text' : '5'},
                            {'fillStyle' : '#ffd33d', 'text' : 'SORRY','textFontSize' : 12, 'textFillStyle' : '#ffffff'},
                            {'fillStyle' : '#d50000', 'text' : '75'},
                            {'fillStyle' : '#38cfad', 'text' : '1'},
                            {'fillStyle' : '#ab4ecb', 'text' : '10'},
                            {'fillStyle' : '#2265d5', 'text' : '5'},
                            {'fillStyle' : '#43a047', 'text' : '100'},
                            {'fillStyle' : '#38cfad', 'text' : '1'},
                            {'fillStyle' : '#ffd33d', 'text' : 'SORRY','textFontSize' : 12, 'textFillStyle' : '#ffffff'},
                            {'fillStyle' : '#ab4ecb', 'text' : '10'},

                        ],
                    'animation' :           // Specify the animation to use.
                        {
                            'type'     : 'spinToStop',
                            'duration' : 10,    // Duration in seconds.
                            'spins'    : 3,     // Default number of complete spins.
                            'callbackFinished' : alertPrize,
                            'stopAngle'         : 0,
                            'callbackSound'    : playSound,   // Function to call when the tick sound is to be triggered.
                            'soundTrigger'     : 'pin'        // Specify pins are to trigger the sound, the other option is 'segment'.
                        },
                    'pins' :				// Turn pins on.
                        {
                            'number'     : 18,
                            'fillStyle'  : 'silver',
                            'outerRadius': 4,
                        }
                });

                // Loads the tick audio sound in to an audio object.
                let audio = new Audio('tick.mp3');


                // This function is called when the sound is to be played.
                function playSound()
                {
                    // Stop and rewind the sound if it already happens to be playing.
                    audio.pause();
                    audio.currentTime = 0;

                    // Play the sound.
                    audio.play();
                }

                // Vars used by the code in this page to do power controls.
                let wheelPower    = 0;
                let wheelSpinning = false;
                let powerLevel=1;

                powerSelected(powerLevel);

                // -------------------------------------------------------
                // Function to handle the onClick on the power buttons.
                // -------------------------------------------------------
                function powerSelected(powerLevel)
                {
                    // Ensure that power can't be changed while wheel is spinning.
                    if (wheelSpinning == false) {
                        // Reset all to grey incase this is not the first time the user has selected the power.
                        document.getElementById('pw1').className = "";
                        document.getElementById('pw2').className = "";
                        document.getElementById('pw3').className = "";

                        // Now light up all cells below-and-including the one selected by changing the class.
                        if (powerLevel >= 1) {
                            document.getElementById('pw1').className = "pw1";
                        }

                        if (powerLevel >= 2) {
                            document.getElementById('pw2').className = "pw2";
                        }

                        if (powerLevel >= 3) {
                            document.getElementById('pw3').className = "pw3";
                        }

                        // Set wheelPower var used when spin button is clicked.
                        wheelPower = powerLevel;

                        // Light up the spin button by changing it's source image and adding a clickable class to it.
                        document.getElementById('spin_button').src = "spin_on.png";
                        document.getElementById('spin_button').className = "clickable";
                    }
                }

                // -------------------------------------------------------
                // Click handler for spin button.
                // -------------------------------------------------------
                function startSpin()
                {
                    // Ensure that spinning can't be clicked again while already running.
                    if (wheelSpinning == false) {
                        wheelSpinning = true;
                        // Based on the power level selected adjust the number of spins for the wheel, the more times is has
                        // to rotate with the duration of the animation the quicker the wheel spins.
                        if (wheelPower == 1) {
                            theWheel.animation.spins = 3;
                        } else if (wheelPower == 2) {
                            theWheel.animation.spins = 6;
                        } else if (wheelPower == 3) {
                            theWheel.animation.spins = 10;
                        }

                        // Disable the spin button so can't click again while wheel is spinning.
                        document.getElementById('spin_button').src       = "spin_off.png";
                        document.getElementById('spin_button').className = "";

                        // Begin the spin animation by calling startAnimation on the wheel object.
                        // $.post( "spinwheel", function( data ) {
                        //     console.log(data);
                        // });

                        let promise = new Promise(function(resolve, reject) {
                            $.ajax({
                                url : "spinwheel",
                                headers: {
                                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content'),
                                },
                                type: "POST",
                                data : {
                                    "user": user
                                }
                            }).done(function(response){ //
                                console.log(response);
                                if(response.status === 1){
                                    theWheel.animation.stopAngle = response.random;
                                    resolve("done!")
                                }else{
                                    reject(new Error("Whoops!"))
                                }

                            });
                        });

                        // resolve runs the first function in .then
                        promise.then(
                            result => {
                            let animation = new Promise(function(resolve) {
                                theWheel.startAnimation();
                                setTimeout(() => resolve("animated!"), 10000);
                            }).then((successMessage) => {
                                console.log("Yay! " + successMessage);
                        console.log(wheelSpinning);

                    });

                    }, // shows "done!" after 1 second
                        error => alert(error) // doesn't run
                    );

                        // $.ajax({
                        //     url : "spinwheel",
                        //     headers: {
                        //         "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content'),
                        //     },
                        //     type: "POST",
                        //     data : {
                        //         "user": user
                        //     }
                        // }).done(function(response){ //
                        //     console.log(response);
                        //     if(response.status === 1){
                        //         console.log(response.random);
                        //         theWheel.animation.stopAngle = response.random;
                        //     }
                        // });
                        // console.log(theWheel.animation);






                    }
                }

                // -------------------------------------------------------
                // Function for reset button.
                // -------------------------------------------------------
                function resetWheel()
                {
                    theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
                    theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
                    theWheel.draw();                // Call draw to render changes to the wheel.

                    // document.getElementById('pw1').className = "";  // Remove all colours from the power level indicators.
                    // document.getElementById('pw2').className = "";
                    // document.getElementById('pw3').className = "";

                    wheelSpinning = false;
                    // Reset to false to power buttons and spin can be clicked again.
                }

                // -------------------------------------------------------
                // Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters.
                // -------------------------------------------------------
                function alertPrize(indicatedSegment)
                {
                    // Just alert to the user what happened.
                    // In a real project probably want to do something more interesting than this with the result.
                    if (indicatedSegment.text == 'LOOSE TURN') {
                        alert('Sorry but you loose a turn.');
                    } else if (indicatedSegment.text == 'BANKRUPT') {
                        alert('Oh no, you have gone BANKRUPT!');
                    } else {
                        alert("You have won " + indicatedSegment.text);
                    }

                    let reset = new Promise(function(resolve) {
                        setTimeout(() => resolve("reset!"), 1000);
                    }).then((successMessage) => {
                        console.log("Yay! " + successMessage);
                        resetWheel();
                        return "power";
                    }).then((successMessage) => {
                        console.log("Yay! " + successMessage);
                        powerSelected(powerLevel);
                    });


                }
            </script>






@endsection
