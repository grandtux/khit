<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="propeller" content="c2d6e1cd1cf2a7cbb41e8d0391afe0be">
    <title>Spin2Lucky</title>
    <meta name="description" content="Marketplace For Anyone Who Wants to Buy and Sell  Digital Files">
    <link rel="shortcut icon" href="assets/img/favicon.png?v=3">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="{{ asset('assets/css/preload.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/plugins.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.light-blue-500.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/main.css') }}" />
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
    {!! Html::script('js/winwheel.js') !!}
</head>
<style>
    {{--.ms-hero-img-city2 {--}}
        {{--background-image: {{ asset('assets/img/skylin2.jpg') }};--}}
        {{--background-size: cover;--}}
        {{--background-position: center center;--}}
    {{--}--}}
    .responsive {
        width: 100%;
        height: auto;
    }
</style>
@yield('style')
<body>
<div id="ms-preload" class="ms-preload">
    <div id="status">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<div class="sb-site-container">
    <!-- Modal -->
    <div class="modal modal-primary" id="ms-account-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog animated zoomIn animated-3x" role="document">
            <div class="modal-content">
                <div class="modal-header shadow-2dp no-pb">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">
                  <i class="zmdi zmdi-close"></i>
                </span>
                    </button>
                    <div class="modal-title text-center">
                        <span class="ms-logo ms-logo-white ms-logo-sm mr-1">S</span>
                        <h3 class="no-m ms-site-title">Spin
                            <span>2Lucky</span>
                        </h3>
                    </div>
                    <div class="modal-header-tabs">
                        <ul class="nav nav-tabs nav-tabs-full nav-tabs-3 nav-tabs-primary" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#ms-login-tab" aria-controls="ms-login-tab" role="tab" data-toggle="tab" class="withoutripple">
                                    <i class="zmdi zmdi-account"></i> Login</a>
                            </li>
                            <li role="presentation">
                                <a href="#ms-register-tab" aria-controls="ms-register-tab" role="tab" data-toggle="tab" class="withoutripple">
                                    <i class="zmdi zmdi-account-add"></i> Register</a>
                            </li>
                            <li role="presentation">
                                <a href="#ms-recovery-tab" aria-controls="ms-recovery-tab" role="tab" data-toggle="tab" class="withoutripple">
                                    <i class="zmdi zmdi-key"></i> Recovery Pass</a>
                            </li>
                        </ul>
                    </div>
                </div>
                @include('par.auth')
            </div>
        </div>
    </div>
    <header class="ms-header ms-header-white">
        <div class="container container-full">
            <div class="ms-title">
                <a href="{{ url('/') }}">
                    <!-- <img src="assets/img/demo/logo-header.png" alt=""> -->
                    <span class="ms-logo animated zoomInDown animation-delay-5">S</span>
                    <h1 class="animated fadeInRight animation-delay-6">Spin
                        <span>2Lucky</span>
                    </h1>
                </a>
            </div>
            <div class="header-right">
                <div class="share-menu">
                    <ul class="share-menu-list">
                        <li class="animated fadeInRight animation-delay-3">
                            <a href="javascript:void(0)" class="btn-circle btn-google">
                                <i class="zmdi zmdi-google"></i>
                            </a>
                        </li>
                        <li class="animated fadeInRight animation-delay-2">
                            <a href="https://www.facebook.com/khitmarket/" class="btn-circle btn-facebook">
                                <i class="zmdi zmdi-facebook"></i>
                            </a>
                        </li>
                        <li class="animated fadeInRight animation-delay-1">
                            <a href="javascript:void(0)" class="btn-circle btn-twitter">
                                <i class="zmdi zmdi-twitter"></i>
                            </a>
                        </li>
                    </ul>
                    <a href="javascript:void(0)" class="btn-circle btn-circle-primary animated zoomInDown animation-delay-7">
                        <i class="zmdi zmdi-share"></i>
                    </a>
                </div>
                <a href="javascript:void(0)" class="btn-circle btn-circle-primary no-focus animated zoomInDown animation-delay-8" data-toggle="modal" data-target="#ms-account-modal">
                    <i class="zmdi zmdi-account"></i>
                </a>
                <form class="search-form animated zoomInDown animation-delay-9" action="/search" method="get" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input id="search-box" type="text" class="search-input" placeholder="Search..." name="search" />
                    <label for="search-box">
                        <i class="zmdi zmdi-search"></i>
                    </label>
                </form>
                {{--<form action="/search" method="get" enctype="multipart/form-data">--}}
                {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                {{--<div class="input-group">--}}
                {{--<input type="text" class="form-control" placeholder="Search" name="search" id="search">--}}
                {{--<div class="input-group-btn">--}}
                {{--<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i>--}}
                {{--</button>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</form>--}}

                <a href="javascript:void(0)" class="btn-ms-menu btn-circle btn-circle-primary sb-toggle-left animated zoomInDown animation-delay-10">
                    <i class="zmdi zmdi-menu"></i>
                </a>

            </div>
        </div>
    </header>
    <nav class="navbar navbar-static-top yamm ms-navbar ms-navbar-primary">
        <div class="container container-full">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <!-- <img src="assets/img/demo/logo-navbar.png" alt=""> -->
                    <span class="ms-logo ms-logo-sm">S</span>
                    <span class="ms-title">Spin
                <strong>2Lucky</strong>
              </span>
                </a>
            </div>
            @include('menu')
        </div>

        <!-- container -->
    </nav>
@yield("content")

<!-- container -->
    <aside class="ms-footbar">
        <div class="container">
            <div class="row">
                <div class="col-md-4 ms-footer-col">
                    <div class="ms-footbar-block">
                        <h3 class="ms-footbar-title">Sitemap</h3>
                        <ul class="list-unstyled ms-icon-list three_cols">
                            <li>
                                <a href="{{ url('/') }}">
                                    <i class="zmdi zmdi-home"></i> Home</a>
                            </li>
                            <li>
                                <a href="https://spin2lucky.com/page/4">
                                    <i class="zmdi zmdi-edit"></i> Guide</a>
                            </li>

                            <li>
                                <a href="https://spin2lucky.com/page/1">
                                    <i class="zmdi zmdi-favorite-outline"></i> About Us</a>
                            </li>

                            <li>
                                <a href="https://spin2lucky.com/page/2">
                                    <i class="zmdi zmdi-help"></i> FAQ</a>
                            </li>
                            <li>
                                <a href="https://spin2lucky.com/page/3">
                                    <i class="zmdi zmdi-case"></i> Buy</a>
                            </li>

                            <li>
                                <a href="https://spin2lucky.com/page/5">
                                    <i class="zmdi zmdi-time"></i> Terms</a>
                            </li>
                            <li>
                                <a href="https://spin2lucky.com/auth/login">
                                    <i class="zmdi zmdi-lock"></i> Login</a>
                            </li>

                        </ul>
                    </div>
                    <div class="ms-footbar-block">
                        <h3 class="ms-footbar-title">Subscribe</h3>
                        <p class="">Lorem ipsum Amet fugiat elit nisi anim mollit minim labore ut esse Duis ullamco ad dolor veniam velit.</p>
                        <form>
                            <div class="form-group label-floating mt-2 mb-1">
                                <div class="input-group ms-input-subscribe">
                                    <label class="control-label" for="ms-subscribe">
                                        <i class="zmdi zmdi-email"></i> Email Adress</label>
                                    <input type="email" id="ms-subscribe" class="form-control"> </div>
                            </div>
                            <button class="ms-subscribre-btn" type="button">Subscribe</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-5 col-sm-7 ms-footer-col ms-footer-alt-color">
                    <div class="ms-footbar-block">
                        <h3 class="ms-footbar-title text-center mb-2">Lastest Products</h3>
                        <div class="ms-footer-media">
                            @foreach($latests as $latest)
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <a href="{{url('products',$latest->id)}}">
                                            <img class="media-object media-object-circle" src="{{ asset('assets/img/demo/p75.jpg') }}" alt="..."> </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <a href="{{url('products',$latest->id)}}">{{ $latest->name }}</a>
                                        </h4>
                                        <div class="media-footer">
                        <span>
                          <i class="zmdi zmdi-time color-info-light"></i>{{ $latest->created_at->format('d-m-Y') }}</span>
                                            <span>
                          <i class="zmdi zmdi-folder-outline color-warning-light"></i>
                          <a href="{{ url('category/'.$latest->category)}}">{{ $latest->category }}</a>
                        </span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-5 ms-footer-col ms-footer-text-right">
                    <div class="ms-footbar-block">
                        <div class="ms-footbar-title">
                            <span class="ms-logo ms-logo-white ms-logo-sm mr-1">S</span>
                            <h3 class="no-m ms-site-title">Spin
                                <span>2Lucky</span>
                            </h3>
                        </div>
                        <address class="no-mb">
                            <p>
                                <i class="color-danger-light zmdi zmdi-pin mr-1"></i>132 A , Room -2 , 33rd Street </p>
                            <p>
                                <i class="color-warning-light zmdi zmdi-map mr-1"></i> Yangon,Myanmar</p>
                            <p>
                                <i class="color-info-light zmdi zmdi-email mr-1"></i>
                                <a href="mailto:kothura23@gmail.com">kothura23@gmail.com</a>
                            </p>
                            <p>
                                <i class="color-royal-light zmdi zmdi-phone mr-1"></i>+95 945 777 6377 </p>
                            <p>
                                <i class="color-royal-light zmdi zmdi-phone mr-1"></i>+95 997 247 3007 </p>
                        </address>
                    </div>
                    <div class="ms-footbar-block">
                        <h3 class="ms-footbar-title">Social Media</h3>
                        <div class="ms-footbar-social">
                            <a href="https://www.facebook.com/khitmarket/" class="btn-circle btn-facebook">
                                <i class="zmdi zmdi-facebook"></i>
                            </a>
                            <a href="javascript:void(0)" class="btn-circle btn-twitter">
                                <i class="zmdi zmdi-twitter"></i>
                            </a>
                            <a href="javascript:void(0)" class="btn-circle btn-youtube">
                                <i class="zmdi zmdi-youtube"></i>
                            </a>
                            <br>
                            <a href="javascript:void(0)" class="btn-circle btn-google">
                                <i class="zmdi zmdi-google"></i>
                            </a>
                            <a href="javascript:void(0)" class="btn-circle btn-instagram">
                                <i class="zmdi zmdi-instagram"></i>
                            </a>
                            <a href="javascript:void(0)" class="btn-circle btn-github">
                                <i class="zmdi zmdi-github"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </aside>
    <footer class="ms-footer">
        <div class="container">
            <p>Copyright &copy; Spin2Lucky 2020</p>
        </div>
    </footer>
    <div class="btn-back-top">
        <a href="#" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised ">
            <i class="zmdi zmdi-long-arrow-up"></i>
        </a>
    </div>
</div>
<!-- sb-site-container -->
@if(Auth::check())
    <div class="ms-slidebar sb-slidebar sb-left sb-style-overlay" id="ms-slidebar">
        <div class="sb-slidebar-container">
            <header class="ms-slidebar-header">
                <div class="ms-slidebar-login">
                    <a href="javascript:void(0)" class="withripple">
                        <i class="zmdi zmdi-account"></i> {{ Auth::user()->name }} ({{ Auth::user()->points }})</a>
                    {{--<a href="javascript:void(0)" class="withripple">--}}
                    {{--<i class="zmdi zmdi-account-add"></i> Register</a>--}}
                </div>
                <div class="ms-slidebar-title">
                    <form class="search-form" action="/search" method="get" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="search-box-slidebar" type="text" class="search-input" placeholder="Search..." name="search"/>
                        <label for="search-box-slidebar">
                            <i class="zmdi zmdi-search"></i>
                        </label>
                    </form>
                    <div class="ms-slidebar-t">
                        <span class="ms-logo ms-logo-sm">S</span>
                        <h3>Spin
                            <span>2Lucky</span>
                        </h3>
                    </div>
                </div>
            </header>

            <ul class="ms-slidebar-menu" id="slidebar-menu" role="tablist" aria-multiselectable="true">
                <li class="panel" role="tab" id="sch1">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#slidebar-menu" href="#sc1" aria-expanded="false" aria-controls="sc1">
                        <i class="zmdi zmdi-home"></i>Products</a>
                    <ul id="sc1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="sch1">
                        <li>
                            <a href="{{ url('/products/create') }}">Add New Product</a>
                        </li>
                        <li>
                            <a href="{{ url('/user/'.Auth::id().'/products') }}">My Products</a>
                        </li>
                        <li>
                            <a href="{{ url('/mypurchases') }}">My Purchased Product</a>
                        </li>

                    </ul>
                </li>
                <li class="panel" role="tab" id="sch5">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#slidebar-menu" href="#sc5" aria-expanded="false" aria-controls="sc5">
                        <i class="zmdi zmdi-shopping-basket"></i> E-Commerce </a>
                    <ul id="sc5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="sch5">
                        <li>
                            <a href="{{ url('user/sales') }}">My Sale</a>
                        </li>
                        <li>
                            <a href="{{ url('user/purchases') }}">My Buy</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="link" href="{{ url('/messages')}}">
                        <i class="zmdi zmdi-view-compact"></i> Message</a>
                </li>
                @if(Auth::user()->role == "admin")
                    <li class="panel" role="tab" id="sch6">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#slidebar-menu" href="#sc6" aria-expanded="false" aria-controls="sc6">
                            <i class="zmdi zmdi-collection-image-o"></i> Admin </a>
                        <ul id="sc6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="sch6">
                            <li>
                                <a href="{{ url('/tag') }}">Tag List</a>
                            </li>
                            <li>
                                <a href="{{ url('/user') }}">User List</a>
                            </li>
                            <li>
                                <a href="{{ url('/sales') }}">All Sale</a>
                            </li>
                            <li>
                                <a href="{{ url('/admin') }}">Unpub Products</a>
                            </li>
                            <li>
                                <a href="{{ url('/published/products') }}">Pub Products</a>
                            </li>
                            <li>
                                <a href="{{ url('/prepaid/create') }}">Create Prepaid</a>
                            </li>
                            <li>
                                <a href="{{ url('/prepaid') }}">Prepaids</a>
                            </li>
                            <li>
                                <a href="{{ url('/slider') }}">Slider</a>
                            </li>
                            <li>
                                <a href="{{ url('/updatepoints') }}">Point List</a>
                            </li>
                            <li>
                                <a href="{{ url('/commissions/'.\Carbon\Carbon::now()->year) }}">Commissions</a>
                            </li>
                        </ul>
                    </li>

                @endif

                <li>
                    <a class="link" href="{{ url('/auth/logout')}}">
                        <i class="zmdi zmdi-view-compact"></i>Logout</a>
                </li>
            </ul>
            <div class="ms-slidebar-social ms-slidebar-block">
                <h4 class="ms-slidebar-block-title">Social Links</h4>
                <div class="ms-slidebar-social">
                    <a href="https://www.facebook.com/khitmarket/" class="btn-circle btn-circle-raised btn-facebook">
                        <i class="zmdi zmdi-facebook"></i>
                        <span class="badge badge-pink">12</span>
                        <div class="ripple-container"></div>
                    </a>
                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-twitter">
                        <i class="zmdi zmdi-twitter"></i>
                        <span class="badge badge-pink">4</span>
                        <div class="ripple-container"></div>
                    </a>
                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-google">
                        <i class="zmdi zmdi-google"></i>
                        <div class="ripple-container"></div>
                    </a>
                    <a href="javascript:void(0)" class="btn-circle btn-circle-raised btn-instagram">
                        <i class="zmdi zmdi-instagram"></i>
                        <div class="ripple-container"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="ms-slidebar sb-slidebar sb-left sb-style-overlay" id="ms-slidebar">
        <div class="sb-slidebar-container">
            <header class="ms-slidebar-header">
                <div class="ms-slidebar-login">
                    <a href="{{ url('auth/login') }}" class="withripple">
                        <i class="zmdi zmdi-account"></i> Login</a>
                    <a href="{{ url('auth/register') }}" class="withripple">
                        <i class="zmdi zmdi-account-add"></i> Register</a>
                </div>
                <div class="ms-slidebar-title">
                    <form class="search-form" action="/search" method="get" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="search-box-slidebar" type="text" class="search-input" placeholder="Search..." name="search"/>
                        <label for="search-box-slidebar">
                            <i class="zmdi zmdi-search"></i>
                        </label>
                    </form>
                    <div class="ms-slidebar-t">
                        <span class="ms-logo ms-logo-sm">S</span>
                        <h3>Spin
                            <span>2Lucky</span>
                        </h3>
                    </div>
                </div>

            </header>

@endif
{{--<script src="assets/js/plugins.min.js"></script>--}}
{{--<script src="assets/js/app.min.js"></script>--}}
{{--<script src="assets/js/ecommerce.js"></script>--}}
{!! Html::script('assets/js/plugins.min.js') !!}
{!! Html::script('assets/js/app.min.js') !!}
{!! Html::script('assets/js/ecommerce.js') !!}

    @yield('script')
</body>
</html>