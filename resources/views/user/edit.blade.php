@extends("admin.mainframe_admin")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-12"></div>
            <h2>Edit the Point of {{ $user->name }}</h2>
            @include('errors.list')
            <form action="{{ route("user.update", $user->id) }}" method="post">
                <input type="hidden" name="_method" value="PATCH">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                {{ $user->name }}
                {{ $user->description }}
                {{ $user->email }}

                <div class="form-group">
                    <label for="description">Point : </label>{{ $user->points }}
                    <input type="number" class="form-control" id="points" placeholder="Add positive or negative Point" name="points">
                </div>

                <div class="form-group">
                    <select class="form-control input-sm" name="role" id="role">
                        @foreach(config('type.usertype') as $key=>$value)
                            <option value="{{$value}}"
                            @if($value == $user->role)
                                selected="selected"
                            @endif
                                >{{$value}}</option>
                        @endforeach


                    </select><!-- end of Item_Drop_Down -->
                </div>

                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
@endsection