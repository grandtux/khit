@extends("master")
@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 align="center">All Posts</h1>

                @foreach($users as $user)
                    <h2><a href="{{ action('UserController@show', [$user->id] ) }}">{{ $user->name }}</a></h2>
                    <p>{{ $user->email }}</p>
                    <p>{{ $user->points }}</p>

                    <form method="POST" action="{{ route("user.destroy", $user->id) }}" accept-charset="UTF-8">
                        <a type="button" class="btn btn-primary" href="{{ action('UserController@edit', [$user->id] ) }}">Edit</a>
                        <input name="_method" type="hidden" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="submit" class="btn btn-default" value="Delete">
                    </form>
                    <hr>

                @endforeach
                {!! $users->render() !!}
            </div>
        </div>
    </div>
@endsection