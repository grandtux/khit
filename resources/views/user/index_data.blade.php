@extends("admin.theme")
@section("content")

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box"
                    <div class="box-header">
                        <h3 class="box-title">User Management</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>email</th>
                                <th>UserType</th>
                                <th>Joined Date</th>
                                <th>Operation Update</th>
                                <th>Operation Delete</th>


                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>

                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{$user->role}}</td>
                                    <td>{{$user->created_at->format('M d,Y ')}}</td>

                                    <td>
                                        <div class="form-group">
                                            <a type="button" class="btn btn-primary"
                                               href="{{ action('UserController@edit', [$user->id] ) }}">Update</a>
                                        </div>
                                    </td>
                                    <td>
                                        <form method="POST" action="{{ route("user.destroy", $user->id) }}" accept-charset="UTF-8">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-danger delete" value="Delete">
                                        </form>
                                    </td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                "pagingType": "full_numbers"
            });
            $(".delete").click(function(){
                if(confirm("Will you Delete this User?"))
                {
                    return true;
                }
                else{
                    return false;
                }
            });
        });
    </script>
@endsection