@extends("admin.theme")
@section("content")
    <div class="container">
        <div class="row">
            <form action="{{route("profile.update",$user->id)}}" method="POST" accept-charset="UTF-18">
                <input type="hidden" name="_method" value="PATCH" accept-charset="UTF-18">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter  Name"
                           value="{{$user->name}}">
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" id="address" name="address" placeholder="Enter  address"
                           value="{{$user->address}}">
                </div>
                <div class="form-group">
                    <label for="phoneno">Phone Number</label>
                    <input type="text" class="form-control" id="phoneno" name="phoneno" placeholder="Enter  phoneno"
                           value="{{$user->phoneno}}">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
                </div>
                <div class="form-group">
                    <label for="created_at">Join Khit:</label>
                    <label for="created_at">{{$user->created_at->format('M d,Y ')}}</label>

                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary form-control">Submit</button>
                </div>
            </form>
        </div>

    </div>
@endsection