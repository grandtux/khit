@extends("master")
@section("content")
    <div class="container">
        <div class="col-md-9">
        <div class="row">
            <div class="col-md-12">


                <h2>Name: {{ $user->name }}</h2>
                <h4>Email: {{ $user->email }}</h4>
                <h4>Points: {{ $user->points }}</h4>


                @unless ($user->updatepoints->isEmpty())
                    <h5>Points History</h5>
                    @foreach ($user->updatepoints as $updatepoint)

                        <h6>Points {{ $updatepoint->points }}</h6>
                        <h6>Time {{ $updatepoint->created_at }}</h6>

                    @endforeach

                @endunless


            </div>
        </div>
        <div class="col-md-4">
            @if(!Auth::guest() && Auth::user())
                <a class="btn btn-info btn-xs" href="{{route("user.edit",$user->id)}}">Edit</a>
            @endif
        </div>
        </div>
    </div>
@endsection