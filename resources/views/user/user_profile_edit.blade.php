@extends("master")
@section("content")
    <div class="container" xmlns="http://www.w3.org/1999/html">
        <div class="row">
            <div class="col-md-12"></div>
            <h2>Edit the Profile of {{ $user->name }}</h2>
            @include('errors.list')
            <form action="{{ action('UserController@update', [$user->id] ) }}" method="POST" accept-charset="UTF-18">
                <input type="hidden" name="_method" value="PATCH">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label for="title">name</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ $user->name }}"/>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>

        </form>
    </div>
@endsection